# EventType: 27575101
#
# Descriptor: [D*(2010)+ -> (D0 -> (K*(892)- -> (KS0 -> pi+ pi-) pi-) mu+ nu_mu) pi+]cc
#
# NickName: Dst_D0pi,Kstmunu,KS0pi,pipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# # 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# # 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[D*(2010)+ => ^(D0 => (K*(892)- => (KS0 => ^pi+ ^pi-) ^pi-) ^mu+ nu_mu) ^pi+]CC'
# tightCut.Cuts      =    {
#     '[mu+]cc'  : ' goodMuon ' , 
#     '[pi+]cc'  : ' goodPion ' , 
#     '[D0]cc' : ' goodD  ' }
# tightCut.Preambulo += [
#     'inAcc    = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     'goodMuon = ( GPT > 0.25 * GeV ) & ( GP > 2.5 * GeV ) & inAcc ' , 
#     'goodPion  =  ( GPT > 150  * MeV ) & ( GP > 1000  * MeV ) & inAcc ',
#     'goodD = ~GHAS(GBEAUTY, HepMC.ancestors ) ' ]
#
# EndInsertPythonCode
#
# Documentation: Forces the D* decay to D0(K* mu nu). K* is forced to decay to pi- K0bar. K0bar is forced to decay to pi+pi-. Dstar is forced to come from c quarks only.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Irene Celestino
# Email: irene.celestino@cern.ch
# Date: 20241202
# CPUTime: < 1 min
#
Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0
#
Alias MyK*- K*-
Alias MyK*+ K*+
ChargeConj MyK*- MyK*+
#
Alias myK_S0  K_S0
ChargeConj myK_S0 myK_S0
#
Decay D*+sig
  1.000 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig
#
Decay MyD0
  1.000 MyK*- mu+ nu_mu PHOTOS ISGW2;  
Enddecay
CDecay MyantiD0
#
Decay MyK*-
  1.000  myK_S0	pi-   VSS; 
Enddecay
CDecay MyK*+
#
Decay myK_S0
1.000     pi+  pi-     PHSP;
Enddecay
#
End

