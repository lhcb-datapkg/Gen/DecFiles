# EventType: 11565400
#
# Descriptor: {[B0 --> (tau+ -> pi+ pi- pi+ anti-nu_tau) nu_tau ( D*- => (D~0 ==> K+ pi- pi+ pi-) pi- ) ... ]cc}
#
# NickName: Bd_Dststtaunu,Dst=cocktail,K3pi,3pinu3pipi0nu,tauolababar,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal = generation.SignalRepeatedHadronization
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut = signal.TightCut
# tightCut.Decay = '[ [B~0]cc --> (D0 ==> K- pi+ pi- pi+) ... ]CC'
# tightCut.Preambulo += [
#      'from GaudiKernel.SystemOfUnits import MeV'
#     ,'inAcc = ( 0 < GPZ )  &  ( 200 * MeV < GPT ) & ( 900 * MeV < GP ) & in_range ( 1.8 , GETA , 5.2 )'
#     ,"nPiB = GNINTREE(('pi+' == GABSID) & inAcc & ( GNINTREE ( ('KS0' == GABSID) | ('KL0' == GABSID), HepMC.parents)==0 ), HepMC.descendants)"
#     ,"nKB = GNINTREE(('K+' == GABSID) & inAcc, HepMC.descendants)"
#     ,"npB = GNINTREE(('p+' == GABSID) & inAcc , HepMC.descendants)"
#     ,"nMuB = GNINTREE(('mu+' == GABSID) & inAcc & ( GNINTREE ( ('KS0' == GABSID) | ('KL0' == GABSID), HepMC.parents)==0 ), HepMC.descendants)"
#     ,"neB = GNINTREE(('e+' == GABSID) & inAcc & ( GNINTREE ( ('KS0' == GABSID) | ('KL0' == GABSID), HepMC.parents)==0 ), HepMC.descendants)"
#     ,"goodD0 = GINTREE(( 'D0'  == GABSID ) & (GP>19000*MeV) & (GPT>1900*MeV) & ( GNINTREE(( 'K-' == GABSID ) & ( GPT > 350*MeV ) & ( GP > 3900*MeV ) & inAcc, HepMC.descendants) == 1 ) & ( GNINTREE(( 'pi+' == GABSID ) & ( GPT > 200*MeV ) & ( GP > 1900*MeV ) & inAcc, HepMC.descendants) == 3 ))"
#     ,"goodB = ( goodD0 & (nPiB+nKB+nMuB+neB+npB >= 7) )"
# ]
# tightCut.Cuts = {
#     '[B~0]cc': 'goodB'
#     }
# EndInsertPythonCode
#
# Documentation: Sum of B0 -> D** tau nu modes. D** -> D*-(-> D0 pi-)X, D0 -> K3pi. Cuts for B -> D* tau nu, D*->piD0, D0->K3pi, tau-> 3pi analysis.
# EndDocumentation
#
# PhysicsWG: B2SL
# CPUTime: < 1 min
# Tested: Yes
# Responsible: Antonio Romero Vidal
# Email: antonio.romero@usc.es
# Date: 20240510
#
# Tauola steering options
# # The following forces the tau to decay into 3 charged pions (not pi+2pi0)
Define TauolaCurrentOption 1
Define TauolaBR1 1.0
#
Alias      Mytau+         tau+
Alias      Mytau-         tau-
ChargeConj Mytau+         Mytau-
#
Alias      MyD0         D0
Alias      MyAntiD0     anti-D0
ChargeConj MyD0         MyAntiD0
#
Alias      MyD*-        D*-
Alias      MyD*+        D*+
ChargeConj MyD*-        MyD*+
#
Alias      MyD_1+         D_1+
Alias      MyD_1-         D_1-
ChargeConj MyD_1+         MyD_1-
#
Alias      MyD_0*+         D_0*+
Alias      MyD_0*-         D_0*-
ChargeConj MyD_0*+         MyD_0*-
#
Alias      MyD'_1+         D'_1+
Alias      MyD'_1-         D'_1-
ChargeConj MyD'_1+         MyD'_1-
#
Alias      MyD_2*+         D_2*+
Alias      MyD_2*-         D_2*-
ChargeConj MyD_2*+         MyD_2*-
#
Decay B0sig 
# FORM FACTORS as per HFAG PDG10
##  0.00  MyD_0*-    Mytau+  nu_tau         PHOTOS  ISGW2; # x 0.00448*0
  0.001528  MyD'_1-    Mytau+  nu_tau         PHOTOS  ISGW2; # x 0.00463*0.33 ;
  0.0014    MyD_1-     Mytau+  nu_tau         PHOTOS  ISGW2; # x 0.00667*0.21 ;
  0.000348  MyD_2*-    Mytau+  nu_tau         PHOTOS  ISGW2; # x 0.00268*0.13 ;
Enddecay
CDecay anti-B0sig

SetLineshapePW MyD_1+ MyD*+ pi0 2
SetLineshapePW MyD_1- MyD*- pi0 2
#
SetLineshapePW MyD_2*+ MyD*+ pi0 2
SetLineshapePW MyD_2*- MyD*- pi0 2
#
Decay Mytau-
  0.6666        TAUOLA 5;
  0.3333        TAUOLA 8;
Enddecay
CDecay Mytau+
#
Decay MyD0
  1.00      K-           pi+        pi+        pi-  PHOTOS    LbAmpGen DtoKpipipi_v2; # (0.0822 +- 0.0014) incl.;
Enddecay
CDecay MyAntiD0
#
Decay MyD*+
  0.6770    MyD0  pi+                          PHOTOS VSS;
Enddecay
CDecay MyD*-
#
Decay MyD'_1+
  0.33    MyD*+ pi0                          PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyD'_1-
#
Decay MyD_1+
  0.21   MyD*+ pi0                           PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay
CDecay MyD_1-
#
Decay MyD_2*+
  0.13   MyD*+ pi0                          PHOTOS TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
Enddecay
CDecay MyD_2*-
#
End
