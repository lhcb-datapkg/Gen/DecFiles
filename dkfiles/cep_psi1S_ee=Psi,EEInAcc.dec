# EventType: 49152002
#
# Descriptor: psi(1S) -> e+ e-
# NickName: cep_psi1S_ee=Psi,EEInAcc
# Cuts: None
# FullEventCuts: LoKi::FullGenEventCut/cepInAcc
# Production: SuperChic2
#
# InsertPythonCode:
#
# # SuperChic2 options.
# from Configurables import SuperChic2Production
# Generation().Special.addTool(SuperChic2Production)
# Generation().Special.SuperChic2Production.Commands += [
#     "SuperChic2:proc   = 50",    # Psi(1S)[mu,mu] production.
#     "SuperChic2:ymin   = 1.95",  # central system min rapidity
#     "SuperChic2:ymax   = 5.05",  # central system max rapidity
#     "SuperChic2:decays = false"] # Turn off SuperChic2 decays.
#
# # Cuts on the psi(1S).
# from Configurables import LoKi__FullGenEventCut
# Generation().addTool(LoKi__FullGenEventCut, "cepInAcc")
# cepInAcc = Generation().cepInAcc
# cepInAcc.Code = "( count( goodJpsi ) == 1 )"
# cepInAcc.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import GeV, mrad",
#     "inAcc       = in_range ( 1.95 , GETA , 5.050 ) ",
#     "goodEplus  = GINTREE( ( GID == -11 ) & inAcc )",
#     "goodEminus = GINTREE( ( GID ==  11 ) & inAcc )",
#     "goodJpsi = ( ( GABSID == 443 ) & goodEplus & goodEminus )"]
#
# # Keep the CEP process in MCParticles.
# from Configurables import GenerationToSimulation
# GenerationToSimulation("GenToSim").KeepCode = ("( GBARCODE >= 2 )")
# EndInsertPythonCode
#
# Documentation:
# central exclusive production of psi(1S)[e e]
# EndDocumentation
#
# PhysicsWG: EW
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Cristina Sanchez Gras
# Email: cristina.sanchez.gras@cern.ch
# Date: 20200107
#
Decay J/psi
1.0 e+ e- PHOTOS VLL;
Enddecay
End
