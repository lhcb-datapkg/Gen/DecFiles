# EventType: 11166106
#
# Descriptor: {[[B0]nos -> (D~0 => (KS0 -> pi+ pi-) pi+ pi-)(K*(892)0 ->K+ pi-) ]cc,[[B0]os ->(D0 => (KS0 -> pi+ pi-) pi+ pi-)(K*(892)~0 ->K- pi+) ]cc} 
#
# NickName: Bd_D0Kst,KSpipi=B-SVS,D-PHSP,TightCut,LooserCuts
#
# Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '^[Beauty => ^(D~0 => ^(KS0 => ^pi+ ^pi-) ^pi+ ^pi-) ^(K*(892)0 =>^K+ ^pi-) ]CC'
#tightCut.Preambulo += [
#    'GVZ = LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = (in_range(0.005, GTHETA, 0.400))',
#    'goodB0       = (GP > 25000 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.05 * millimeter)',
#    'goodD0       = (GP > 10000 * MeV) & (GPT > 500 * MeV)',
#    'goodKS       = (GP > 4000 * MeV) & (GPT > 250 * MeV)',
#    'goodKst      = (GP > 6000 * MeV) & (GPT >  800 * MeV)',
#    'goodDDaugPi  = (GNINTREE (("pi+" == GABSID) & (GP > 1000 * MeV) & inAcc, 1) > 1.5)',
#    'goodKsDaugPi = (GNINTREE (("pi+" == GABSID) & (GP > 1750 * MeV) & inAcc, 1) > 1.5)',
#    'goodKstPi  = (GNINTREE( ("pi+"==GABSID) & (GP > 2000 * MeV) & (GPT > 98 * MeV) & inAcc, 1) > 0.5)',
#    'goodKstK   = (GNINTREE( ("K+"==GABSID) & (GP > 2000 * MeV) & (GPT > 98 * MeV) & inAcc, 1) > 0.5)',
#]
#tightCut.Cuts = {
#    'Beauty'          : 'goodB0', 
#    '[D0]cc'          : 'goodD0 & goodDDaugPi',
#    '[K*(892)0]cc'    : 'goodKst & goodKstPi & goodKstK',
#    '[KS0]cc'         : 'goodKS & goodKsDaugPi'
#    }
#EndInsertPythonCode
#
# Documentation: K*0 forced to K+ pi-, D~0 forced to (Kshort-> pi pi) pi+ pi-, 
#                Phase Space model for D decay,
#                Only favoured mode for B->DK* (SVS model) and looser tight cuts
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: <1min
# Responsible: Hannah Pullen
# Email: hannah.louise.pullen@cern.ch
# Date: 20190509
#
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
Alias      Myanti-D0  anti-D0
Alias      MyD0       D0
ChargeConj MyD0       Myanti-D0
Alias      MyKs       K_S0
ChargeConj MyKs       MyKs
#
Decay B0sig
# SVS model
  1.000    MyK*0      Myanti-D0   SVS;  
Enddecay 
CDecay anti-B0sig
#
# PhaseSpace model in D0 decay
Decay MyD0
  1.000    MyKs       pi-         pi+          PHSP;
Enddecay
CDecay Myanti-D0
#
Decay MyK*0
  1.000    K+         pi-         VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyKs
  1.000    pi+        pi-         PHSP;
Enddecay
#       
End
#
