# EventType: 16366111
# 
# Descriptor: [Sigma_b0 -> (Lambda_c+ -> p+ K- pi+) (Lambda0 -> p+ pi-) anti-p-]cc
# 
# NickName: Sigmab_LcpbarLambda,pKpi,ppi=DecProdCut
#
# Cuts: DaughtersInLHCb
#
# Documentation: Sigma_b0 -> Lc Lambda0 pbar with Lc -> p K pi and Lambda0 -> p pi decay products in LHCb's acceptance. Includes Delta and K*0 resonance in Lambda_c decay
# EndDocumentation
#
# PhysicsWG: Onia
# Tested: Yes
# CPUTime: <1min
# Responsible: Zehua Xu 
# Email: zehua.xu@cern.ch
# Date: 20241128
#

Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
Alias      MyK*0          K*0
Alias      Myanti-K*0     anti-K*0
ChargeConj MyK*0          Myanti-K*0
#
Alias      MyLambda     Lambda0
Alias      MyantiLambda anti-Lambda0
ChargeConj MyLambda     MyantiLambda
#
Decay Sigma_b0sig
1.000        MyLambda_c+ MyLambda anti-p-     PHSP;
Enddecay
CDecay anti-Sigma_b0sig
#
Decay MyLambda_c+
 0.008600000 Delta++ K-                                      PHSP;
 0.010700000 p+      Myanti-K*0                              PHSP;
 0.025400000 p+      K-      pi+                             PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyK*0
  1.000      K+  pi-                          VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyLambda
  1.000        p+      pi-                PHSP;
Enddecay
CDecay MyantiLambda
#
End
