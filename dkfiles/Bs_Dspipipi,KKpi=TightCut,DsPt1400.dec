# EventType: 13266069
#
# Descriptor: [[B_s0]cc ==> (D_s- ==> K+ K- pi-) pi+ pi+ pi- ]CC
#
# NickName: Bs_Dspipipi,KKpi=TightCut,DsPt1400
# Cuts: LoKi::GenCutTool/TightCut
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay     = '^[[B_s0]cc ==> ^(D_s- ==> ^K+ ^K- ^pi-) ^pi+ ^pi+ ^pi- ]CC'
#tightCut.Cuts      =    {
#'[B_s0]cc'   : " (goodB ) ",
#'[D_s-]cc'   : "(goodDs)",
#'[K+]cc'    : ' goodTrack & (GPT > 1200 * MeV) ' ,
#'[pi+]cc'   : ' goodTrack  '
#}
#tightCut.Preambulo += [
#'from GaudiKernel.SystemOfUnits import  MeV',
#'inAcc = ( in_range( 0.010, GTHETA, 0.400) ) & ( in_range( 1.6, GETA, 5.6) )  ',
#'goodTrack  = ( GPT > 100  * MeV ) &  ( inAcc )' ,
#"goodB  = ( GPT > 1500 * MeV ) & ( GP > 30000 * MeV ) ",
#"goodDs = ( GPT > 1400 * MeV )"
#]
#EndInsertPythonCode
# Documentation: Bs->Dspipipi, Ds->KKpi, tight generator level cuts
# EndDocumentation
##
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Carmen Giugliano
# Email: carme.giugliano@gmail.com
# Date: 20200326
# CPUTime: 1 min
#
Alias      MyD_s-     D_s-
Alias      MyD_s+     D_s+
ChargeConj MyD_s+     MyD_s-
#
Alias      Myf_0      f_0
ChargeConj Myf_0      Myf_0
#
Alias      Myrho0     rho0
ChargeConj Myrho0     Myrho0
#
Alias      MyK_1-     K_1-
Alias      MyK_1+     K_1+
ChargeConj MyK_1+     MyK_1-

noPhotos

LSNONRELBW MyK_1+
BlattWeisskopf MyK_1+ 0.0
Particle MyK_1+ 1.18 0.4
ChangeMassMin MyK_1+ 0.
ChangeMassMax MyK_1+ 3.5

LSNONRELBW MyK_1-
BlattWeisskopf MyK_1- 0.0
Particle MyK_1- 1.18 0.4
ChangeMassMin MyK_1- 0.
ChangeMassMax MyK_1- 3.5

LSNONRELBW Myrho0
BlattWeisskopf Myrho0 0.0
Particle Myrho0 0.775 0.15
ChangeMassMin Myrho0 0.
ChangeMassMax Myrho0 3.3

LSNONRELBW Myf_0
BlattWeisskopf Myf_0 0.0
Particle Myf_0 0.550 0.400
ChangeMassMin Myf_0 0.
ChangeMassMax Myf_0 3.3

#
Decay B_s0sig
0.050     MyD_s-  pi+ pi+ pi-         PHSP;
0.950     MyK_1+  MyD_s-           PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyK_1+
0.900   Myrho0 pi+                          PHSP;
0.100   Myf_0 pi+                           PHSP;
Enddecay
CDecay MyK_1-
#
Decay MyD_s-
1.000     K+    K-     pi-          D_DALITZ;
Enddecay
CDecay MyD_s+
#
Decay Myf_0
1.000 pi+   pi-                  PHSP;
Enddecay
#
Decay Myrho0
1.000 pi+  pi-                    PHSP;
Enddecay
#
End
