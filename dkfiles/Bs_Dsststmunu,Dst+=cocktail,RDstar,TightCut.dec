# 
#
# EventType: 13674000
#
# Descriptor: [B_s0 -> (D*_s2- -> (D*(2010)- -> (D~0 -> K+ pi-) pi-) anti-K0) mu+ nu_mu]cc
#
# NickName: Bs_Dsststmunu,Dst+=cocktail,RDstar,TightCut
# Cuts: 'LoKi::GenCutTool/TightCut'

# InsertPythonCode:

#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay = "[ (Beauty) ==> ^(D~0 -> ^K+ ^pi- {gamma} {gamma} {gamma}) ^mu+ nu_mu {X} {X} {X} {X} {X} {X} {X} {X} ]CC"
#tightCut.Preambulo += [
#  "from LoKiCore.functions import in_range"  ,
#  "from GaudiKernel.SystemOfUnits import GeV, MeV"  ,
#  "piKP     = GCHILD(GP,('K+' == GABSID )) + GCHILD(GP,('pi-' == GABSID ))" ,
#  "piKPT     = GCHILD(GPT,('K+' == GABSID )) + GCHILD(GPT,('pi-' == GABSID ))" ,
#]
#tightCut.Cuts      =    {
# '[pi+]cc'   : " ( ( GPX / GPZ ) < 0.38 ) & ( ( GPY / GPZ ) < 0.28 ) & ( ( GPX / GPZ ) > - 0.38 ) & ( ( GPY / GPZ ) > - 0.28 ) & ( GTHETA > 0.01 ) & ( GPT > 250 * MeV )" ,
# '[K-]cc'   : " ( ( GPX / GPZ ) < 0.38 ) & ( ( GPY / GPZ ) < 0.28 ) & ( ( GPX / GPZ ) > - 0.38 ) & ( ( GPY / GPZ ) > - 0.28 ) & ( GTHETA > 0.01 ) & ( GPT > 250 * MeV )" ,
# '[mu+]cc'  : " ( ( GPX / GPZ ) < 0.38 ) & ( ( GPY / GPZ ) < 0.28 ) & ( ( GPX / GPZ ) > - 0.38 ) & ( ( GPY / GPZ ) > - 0.28 ) & ( GTHETA > 0.01 ) & ( GP > 2950* MeV) ",
# '[D~0]cc'   : "( piKP > 15000 * MeV ) & (piKPT > 2450 * MeV)"
#    }
# EndInsertPythonCode
#
# Documentation: Sum of Bs -> Ds** mu nu modes. Ds** -> D*+ X, D* -> D0 pi, D0 -> K pi. Cuts for B -> D* tau nu, tau-> mu  Run2 analysis.
# EndDocumentation
#
# CPUTime:< 1min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Svende Braun
# Email: svende.braun@cern.ch
# Date: 20200928
#
Alias      MyD0         D0
Alias      MyAntiD0     anti-D0
ChargeConj MyD0         MyAntiD0
#
Alias      MyD*-        D*-
Alias      MyD*+        D*+
ChargeConj MyD*-        MyD*+
#
Alias      MyD_s1+         D_s1+
Alias      MyD_s1-         D_s1-
ChargeConj MyD_s1+         MyD_s1-
#
Alias      MyD_s0*+         D_s0*+
Alias      MyD_s0*-         D_s0*-
ChargeConj MyD_s0*+         MyD_s0*-
#
Alias      MyD'_s1+         D'_s1+
Alias      MyD'_s1-         D'_s1-
ChargeConj MyD'_s1+         MyD'_s1-
#
Alias      MyD_s2*+         D_s2*+
Alias      MyD_s2*-         D_s2*-
ChargeConj MyD_s2*+         MyD_s2*-
#
#
Decay B_s0sig
0.0070   MyD'_s1-   mu+    nu_mu        PHOTOS  ISGW2;
0.0070   MyD_s2*-   mu+    nu_mu        PHOTOS  ISGW2;
Enddecay
CDecay anti-B_s0sig
#
Decay MyD'_s1-
0.5000   MyD*- anti-K0                      VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay
CDecay MyD'_s1+
#
Decay MyD_s2*-
0.0500    MyD*- anti-K0                         TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
Enddecay
CDecay MyD_s2*+
#
Decay MyD*-
1.0       MyAntiD0   pi-                   VSS;
Enddecay
CDecay MyD*+
#
Decay MyAntiD0
1.00   K+  pi-                           PHOTOS PHSP;
Enddecay
CDecay MyD0
#
End
