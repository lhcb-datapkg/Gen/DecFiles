# EventType: 15576007
#
# Descriptor: [Lambda_b0 -> (Lambda_c(2625)+ -> (Sigma_c++ -> (Lambda_c+ -> p+ K- pi+) pi+) pi-) mu- anti-nu_mu]cc
#
# NickName: Lb_Lc2765munu,Sigcpppi,Lcpi,pKpi=LHCbAcceptance
#
# Cuts: LHCbAcceptance
#
# Documentation: Lb decaying to Lambda_c(2765) mu- anti-nu_mu. Lc2765 not defined in evtgen, so we modify the Lc2625 mass and use with Lb2Baryonlnu. 
# Lc(2765) then forced to Sigma_c++ pi-, Sigma_c forced to Lc pi
#
# EndDocumentation
#
# CPUTime: < 1min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Scott Ely
# Email: seely@syr.edu
# Date: 20221102
#
# ParticleValue: "Lambda_c(2625)+ 104124   104124   1.0   2.7666000 -0.05  Lambda_c(2625)+ 0 0.2", "Lambda_c(2625)~- -104124   -104124   -1.0   2.7666000 -0.05  anti-Lambda_c(2625)- 0 0.2"
#
Alias		MyLambda_c+			Lambda_c+
Alias		MyAntiLambda_c-			anti-Lambda_c-
ChargeConj	MyLambda_c+			MyAntiLambda_c-
#
Alias		MyLambda_c(2625)+		Lambda_c(2625)+
Alias		MyAntiLambda_c(2625)-		anti-Lambda_c(2625)-
ChargeConj	MyLambda_c(2625)+		MyAntiLambda_c(2625)-
#
Alias		MySigma_c++			Sigma_c++
Alias		MyAntiSigma_c--			anti-Sigma_c--
ChargeConj	MySigma_c++			MyAntiSigma_c--
#
Decay Lambda_b0sig
 1.00     MyLambda_c(2625)+	mu-	anti-nu_mu		 Lb2Baryonlnu 1 1 1 1;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c(2625)+
 1.0	 MySigma_c++	pi-	PHSP;
Enddecay
CDecay MyAntiLambda_c(2625)-
#
Decay MySigma_c++
 1.0	MyLambda_c+	pi+	PHSP;
Enddecay
CDecay MyAntiSigma_c--
#
Decay MyLambda_c+
 1.0     p+	K-	pi+	PHSP;
Enddecay
CDecay MyAntiLambda_c-
#
End
