# EventType: 16105931
#
# NickName: Xibm_XimPhi,XimToLambdaPi,PhiToKK=TightCut,AngularCut
#
# Descriptor: [Xi_b- -> (Xi- -> (Lambda0 -> p+ pi-) pi-) (phi(1020) -> K+ K-)]cc
#
# Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
#
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#gen = Generation()
#gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay = "[^(Xi_b- ==> (Xi- ==> (Lambda0 ==> p+ pi-) pi-) (phi(1020) ==> K+ K-) )]CC"
#tightCut.Preambulo += [
#"from GaudiKernel.SystemOfUnits import MeV, meter",
#"InAcc = ( in_range ( 0.005 , GTHETA , 0.400 ) & in_range ( 1.8 , GETA , 5.2 ) )",
#"InAcc_DD = in_range ( 0.001 , GTHETA , 0.390 ) & in_range ( 1.8 , GETA , 7.0 )",
#"decay_position = in_range( -1.1 * meter, GFAEVX ( LoKi.GenVertices.PositionZ(), 100 * meter ), 3 * meter ) ",
#"good_pi = ( GP > 2500 * MeV ) & ( GPT > 200 * MeV) & InAcc",
#"good_phiK = ( GP > 1000 * MeV ) & ( GPT > 500 * MeV) & InAcc",
#"good_XimPim = ( GP > 1000 * MeV ) & ( GPT > 100 * MeV) & InAcc_DD",
#"good_L0p = ( GPT > 500 * MeV) & InAcc_DD",
#"good_L0pi = ( GPT > 100 * MeV) & InAcc_DD",
#"good_L0 = ( ( 'Lambda0' == GABSID ) & (GNINTREE( good_L0p, 1 ) > 0 ) & (GNINTREE( good_L0pi, 1 ) > 0 ) & decay_position )",
#"good_Xim = ( ( 'Xi-' == GABSID ) & (GNINTREE( good_L0, 1 ) > 0 ) & ( GNINTREE( good_XimPim, 1 ) > 0 ) & decay_position )",
#"good_phi = ( ( 'phi(1020)' == GABSID ) & (GNINTREE( good_phiK, 1 ) > 1 ) )",
#"good_Xibm = ( ( 'Xi_b-' == GABSID ) & (GNINTREE( good_Xim, 1 ) > 0 ) & (GNINTREE( good_phi, 1 ) > 0 )  )",
#]
#
#tightCut.Cuts = {
#'[Xi_b-]cc' : "good_Xibm"
#}
#
#EndInsertPythonCode
#
# Documentation: XibmToXimPhi, Lambda0 forced into p+ pi-, phi(1020) forced into K+ K-; TightCut
# EndDocumentation
#
# PhysicsWG: BnoC
#
# Tested: Yes
# Responsible: Miroslav Saur
# Email: miroslav.saur@cern.ch
# Date: 20240327
# CPUTime: < 1 min
#
#
Alias      MyXi     Xi-
Alias      Myanti-Xi anti-Xi+
ChargeConj Myanti-Xi MyXi
#
Alias      MyLambda      Lambda0
Alias      Myanti-Lambda anti-Lambda0
ChargeConj Myanti-Lambda MyLambda
# 
Alias      MyPhi      phi
ChargeConj MyPhi      MyPhi
# 
Decay Xi_b-sig 
1.000    MyXi          MyPhi      PHSP;
Enddecay
CDecay anti-Xi_b+sig
#
Decay MyXi
  1.000     MyLambda   pi-      HELAMP   0.551  0.0  0.834  0.0;
Enddecay
CDecay Myanti-Xi
#
Decay MyLambda
  1.000     p+   pi-             HELAMP   0.936   0.0   0.351   0.0;
Enddecay
CDecay Myanti-Lambda
#
Decay MyPhi
  1.000     K+   K-             PHSP;
Enddecay
#
End
