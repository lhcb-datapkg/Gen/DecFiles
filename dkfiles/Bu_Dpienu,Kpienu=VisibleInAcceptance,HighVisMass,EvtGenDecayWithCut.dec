# EventType: 12585042
#
# Descriptor: [B+ -> (D- -> K+ pi- e- anti-nu_e) pi+ e+ nu_e]cc
# NickName: Bu_Dpienu,Kpienu=VisibleInAcceptance,HighVisMass,EvtGenDecayWithCut
# Cuts: LoKi::GenCutTool/DecProdCut
#
# Documentation: D chain background for B+ -> Kpipiee. The visible mass is required to be larger than 4500 MeV and the visible daughters as required to be inside the LHCb acceptance
# EndDocumentation
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay   = '[^(B+ => ^(D- => ^K+ ^pi- ^e- ^nu_e~) ^pi+ ^e+ ^nu_e)]CC'
# evtgendecay.HighVisMass.Cuts    = { '[B+]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += [
#    "BPX2 = (GCHILD(GPX,'K+' == GABSID) + GCHILD(GPX,'pi+' == GID) + GCHILD(GPX,'pi-' == GID) + GCHILD(GPX,'e+' == GID) + GCHILD(GPX,'e-' == GID))**2",
#    "BPY2 = (GCHILD(GPY,'K+' == GABSID) + GCHILD(GPY,'pi+' == GID) + GCHILD(GPY,'pi-' == GID) + GCHILD(GPY,'e+' == GID) + GCHILD(GPY,'e-' == GID))**2",
#    "BPZ2 = (GCHILD(GPZ,'K+' == GABSID) + GCHILD(GPZ,'pi+' == GID) + GCHILD(GPZ,'pi-' == GID) + GCHILD(GPZ,'e+' == GID) + GCHILD(GPZ,'e-' == GID))**2",
#    "BPE2 = (GCHILD(GE,'K+' == GABSID) + GCHILD(GE,'pi+' == GID) + GCHILD(GE,'pi-' == GID) + GCHILD(GE,'e+' == GID) + GCHILD(GE,'e-' == GID))**2",
#    "visMass  = (BPE2 - BPX2 - BPY2 - BPZ2) > 4500*4500",
# ]
#
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'DecProdCut' ) 
# tightCut  = gen.SignalRepeatedHadronization.DecProdCut
# tightCut.Decay    = evtgendecay.HighVisMass.Decay
# tightCut.Cuts     = {
#     '[K+]cc'     : "ACC",
#     '[pi-]cc'    : "ACC",
#     '[e+]cc'     : "ACC",
#     '[e-]cc'     : "ACC", 
#    }
# tightCut.Preambulo += [
#    "ACC  = in_range(0.0075, GTHETA, 0.400)",
#    ]
# EndInsertPythonCode
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Tobias Tekampe
# Email: ttekampe@cern.ch
# Date: 20180604
# CPUTime: < 1 min 
#
Alias        MyD-         D-
Alias        MyD+         D+
ChargeConj   MyD+         MyD-
#	
Decay B+sig
1.000        MyD- pi+ e+  nu_e          PHOTOS GOITY_ROBERTS;
Enddecay
CDecay B-sig
#
Decay MyD-
1.000        K+ pi- e-  anti-nu_e       PHSP;
Enddecay
CDecay MyD+
#	
End
#
