# EventType: 12195035
#
# Descriptor: [B+ -> (D*(2010)+ => (D0 -> K- pi+) pi+) (D~0 -> K+ pi-)]cc
#
# NickName: Bu_DstD0,D0pi+,Kpi,Kpi,Cut=DecProdCut,pCut1600MeV
#
# Cuts: DaughtersInLHCbAndWithMinP
#
# ExtraOptions: TracksInAccWithMinP
#
# Documentation: B+ -> D*+ D0b, where the D0b -> K pi and the D*+ -> D0 pi+ with D0 -> K pi. Daughters in LHCb acceptance
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: < 1ms
# Responsible: Jonah Blank
# Email: jonah.evan.blank@cern.ch
# Date: 20230323


# -----------------------
# DEFINE THE D*, D0 AND D0bar
# -----------------------
Alias MyD*+ D*+
Alias MyD*- D*-
ChargeConj MyD*+ MyD*-
Alias      MyD0fromD*  D0
Alias      Myanti-D0fromD*  anti-D0
ChargeConj MyD0fromD*  Myanti-D0fromD*
Alias      MylonelyD0  D0
Alias      Mylonelyanti-D0  anti-D0
ChargeConj MylonelyD0  Mylonelyanti-D0


# ---------------
# DECAY OF THE B+
# ---------------
Decay B+sig
  1.000     MyD*+       Mylonelyanti-D0            SVS;
Enddecay
CDecay B-sig

# ---------------
# DECAY OF THE D*
# ---------------
Decay MyD*+
  1.000   MyD0fromD*  pi+                 VSS;
Enddecay
CDecay MyD*-
  

# ---------------
# DECAY OF THE D0
# ---------------
Decay MyD0fromD*
  1.000 K-  pi+                    PHSP;
Enddecay
CDecay Myanti-D0fromD*
Decay Mylonelyanti-D0
  1.000 K+  pi-                   PHSP;
Enddecay
CDecay MylonelyD0


End
