# EventType: 25203000
#
# Descriptor: ${Lc}[Lambda_c+ ==> ${Lcp}p+ ${LcK}K- ${Lcpi}pi+]CC
#
# NickName: Lc_pKpi-res=LHCbAcceptance
#
# Cuts: LHCbAcceptance
#
# Documentation: Lambda_c+ -> p K pi including resonances
# EndDocumentation
#
# CPUTime: < 1 min
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Michael Wilkinson (Syracuse University)
# Email: miwilkin@syr.edu
# Date: 20190225
#
# Define K*(892)
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
# Define Delta++
Alias      MyDelta++      Delta++
Alias      Myanti-Delta-- anti-Delta--
ChargeConj MyDelta++      Myanti-Delta--
#
# Define Lambda(1520)0
Alias      MyLambda(1520)0       Lambda(1520)0
Alias      Myanti-Lambda(1520)0  anti-Lambda(1520)0
ChargeConj MyLambda(1520)0       Myanti-Lambda(1520)0
#
Decay Lambda_c+sig
    0.01940       p+              Myanti-K*0     PHSP;
    0.01070       MyDelta++       K-             PHSP;
    0.02200       MyLambda(1520)0 pi+            PHSP;
    0.03400       p+              K-         pi+ PHSP;
Enddecay
CDecay anti-Lambda_c-sig
#
Decay MyK*0
    1.00000       K+              pi-            VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyDelta++
    0.99400       p+              pi+            PHSP;
Enddecay
CDecay Myanti-Delta--
#
Decay MyLambda(1520)0
    0.45000       p+              K-             PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
End
#
