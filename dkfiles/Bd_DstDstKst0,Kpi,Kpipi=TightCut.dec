# EventType: 11198410
# NickName: Bd_DstDstKst0,Kpi,Kpipi=TightCut
# Descriptor: [B0 -> (D*+ -> (D+ -> K- pi+ pi+) pi0) (D*- -> (D~0 -> K+ pi-) pi-) (K*0 -> K+ pi-)]cc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
##
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool , 'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[Beauty => (D*(2010)+ => (D+ => ^K- ^pi+ ^pi+) X0 ) (D*(2010)- => (D~0 => ^K+ ^pi-) pi-) (K*(892)0=> ^K+ ^pi-) ]CC"
# tightCut.Preambulo += [ "from LoKiCore.functions import in_range",
#                           "from GaudiKernel.SystemOfUnits import  GeV, mrad",
#                         ]
#tightCut.Cuts      =    {
# '[pi+]cc'   : " in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV )",
# '[K-]cc'   : "  in_range( 0.010 , GTHETA , 0.400 )  & ( GPT > 250 * MeV )"
#    }
# EndInsertPythonCode
# Documentation: Decay B0 -> D*+ D*- K*0, D*+ -> D+ pi0/gamma , D+ -> K-pi+pi+, D*- -> D~0pi-, D~0->Kpi, -K*->Kpi
# EndDocumentation
# CPUTime: < 1 min
#
# Date:   20190528
# Responsible: Matt Rudolph
# Email: matthew.scott.rudolph@cern.ch
# PhysicsWG: B2OC
# Tested: Yes

Alias My_K*0      K*0
Alias My_anti-K*0 anti-K*0
Alias My_D*+  D*+
Alias My_D*-  D*-
Alias My_D*+_D0  D*+
Alias My_D*-_D0  D*-
Alias My_D+  D+
Alias My_D-  D-
Alias My_D0    D0
Alias My_anti-D0   anti-D0
ChargeConj My_D*- My_D*+
ChargeConj My_D*-_D0 My_D*+_D0
ChargeConj My_K*0 My_anti-K*0
ChargeConj My_D- My_D+
ChargeConj My_D0   My_anti-D0

Decay My_K*0
  1.0   K+   pi-        VSS;
Enddecay
CDecay My_anti-K*0

#D- Decay
Decay My_D+
  1.0 K- pi+ pi+ D_DALITZ;
Enddecay
CDecay My_D-
#
Decay My_D0
  1.0   K-    pi+       PHSP;
Enddecay
CDecay My_anti-D0
#


#
Decay My_D*-
  0.307 My_D- pi0  VSS;
  0.016 My_D- gamma  VSP_PWAVE;
Enddecay
CDecay My_D*+
#
Decay My_D*-_D0
  1.0 My_anti-D0 pi-  VSS;
Enddecay
CDecay My_D*+_D0

Decay B0sig
  1.0 My_D*+ My_D*-_D0 My_K*0 PHSP;
Enddecay
CDecay anti-B0sig

End
