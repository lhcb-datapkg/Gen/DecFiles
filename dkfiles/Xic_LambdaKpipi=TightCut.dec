# EventType: 26105192
#
# Descriptor: [Xi_c+ -> (Lambda0 -> p+ pi-) K- pi+ pi+]cc
#
# NickName: Xic_LambdaKpipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 2.3 min
#
# Documentation:
# Phase-space decay of Xic+ to Lambda0 K- pi+ pi+
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# Generation().SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
#
# tightCut = Generation().SignalPlain.TightCut
# tightCut.Decay     = '^[Xi_c+ => ^(Lambda0 => ^p+ pi-) K- pi+ pi+]CC'
# tightCut.Preambulo += [
#    'GVZ           =  LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import GeV, MeV, millimeter ',
#    'inAcc         =  (in_range ( 0.010 , GTHETA , 0.400 )) ',
#    'inEta         =  (in_range ( 1.8  , GETA   , 5.2 )) ' ,    
#    'endZ          =  (GFAEVX(abs(GVZ), 0) < 2400.0 * millimeter)',
#    'XicDaugPiK    =  (GNINTREE( (("pi+"==GABSID) | ("K+"==GABSID)) & (GP > 1.9 * GeV) & (GPT > 140 * MeV) & inAcc & inEta, 1) > 2.5) ',
#    'LamDaugPi     =  (GNINTREE( ("pi+"==GABSID) & (GP > 1.9 * GeV), 1) > 0.5) ',
#    'protonMom     =  ( GP > 7.0 * GeV ) & ( GPT > 340 * MeV ) ',
#    'goodXic       =  ( GP > 19.0 * GeV ) ',
#    'goodLam       =  ( GP > 9.5 * GeV ) & ( GPT > 450 * MeV ) & endZ '
# ]
# tightCut.Cuts = {
#    '[Xi_c+]cc': 'goodXic & XicDaugPiK ',
#    '[Lambda0]cc': 'goodLam & LamDaugPi ',
#    '[p+]cc': 'protonMom '
# }
#
# EndInsertPythonCode
#
# PhysicsWG:   Charm
# Tested:      Yes
# Responsible: Joan Ruiz Vidal
# Email:       joan.ruiz.vidal@cern.ch
# Date:        20200408
#

Alias      MyLambda0      Lambda0
Alias      MyAntiLambda0  anti-Lambda0
ChargeConj MyLambda0      MyAntiLambda0

Decay MyLambda0
  1.000     p+   pi-             PHSP;
Enddecay
CDecay MyAntiLambda0
#
Decay Xi_c+sig
  1.0  K- MyLambda0  pi+ pi+  PHSP;
Enddecay
CDecay anti-Xi_c-sig
#
End 
