# EventType: 12513005
#
# Descriptor: [B+ -> (f_0(500) -> pi+ pi-) mu+ nu_mu]cc
#
# NickName: Bu_f0_500munu,PiPi=TightCut,ISGW2
#
# Documentation: Decay file for [B+ -> (f_0(500) -> pi+ pi-) mu+ nu_mu]cc. The f_0(500) particle (PDG notation) is called sigma_0 in EvtGen.
# EndDocumentation
#
# Cuts: 'LoKi::GenCutTool/TightCut'
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen = Generation().SignalRepeatedHadronization
# gen.addTool( LoKi__GenCutTool, "TightCut" )
# SignalFilter = gen.TightCut
# SignalFilter.Decay = "[B+ => (sigma_0 --> ^pi+ ^pi- ...) ^mu+ nu_mu]CC"
# SignalFilter.Preambulo += [
#   "from GaudiKernel.SystemOfUnits import  GeV",
#   "inAcc                = in_range ( 0.005 , GTHETA , 0.400 ) &  in_range ( 1.9 , GETA , 5.1 )", 
#   "muCuts               = (GP > 5 * GeV) &  (GPT > 1.2 * GeV) & inAcc",
#   "piCuts               = (GP > 1.5 * GeV) & (GPT > 0.05 * GeV) & inAcc",
#   ]
# SignalFilter.Cuts =  { "[mu+]cc" : "muCuts",
#                        "[pi+]cc" : "piCuts" }
# EndInsertPythonCode
#
# CPUTime: <1min
# PhysicsWG: B2SL
# Tested: Yes
# Responsible:   Michel De Cian
# Email: michel.de.cian@cern.ch
# Date: 20220630
#
Alias           Myf0            sigma_0
ChargeConj      Myf0            Myf0
#
Decay B+sig
  1.     Myf0        mu+    nu_mu    PHOTOS  ISGW2;
Enddecay
CDecay B-sig
#
Decay Myf0
 1.00		pi+	pi-			PHSP;
Enddecay
#
End
#
