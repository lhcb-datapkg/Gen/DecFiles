# EventType: 15204101
#
# Descriptor: [Lambda_b0  -> K+ K- (Lambda0 -> p+ pi-)]cc
#
# NickName: Lb_LambdaKK=phsp,TightTightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = "[Lambda_b0  ==> ^K+ ^K- ^(Lambda0 ==> p+ pi-)]CC"
# tightCut.Preambulo += [
# "from GaudiKernel.SystemOfUnits import MeV, millimeter",
# "InAcc = in_range ( 0.005 , GTHETA , 0.400 ) & in_range ( 2.0 , GETA , 5.0 )",
# "goodLb = ( GPT > 1090 * MeV )",
# "goodLz = ( GP > 9490 * MeV ) & ( GPT > 390 * MeV) & (GFAEVX( abs( GVZ  ) , 0 ) < 2500 * millimeter) & InAcc",
# "goodHpHm = ( GP > 2490 * MeV ) & ( GPT > 290 * MeV) & InAcc",
#]
#tightCut.Cuts = {
# '[Lambda_b0]cc': "goodLb" , 
# '[Lambda0]cc' : "goodLz",
# '[K+]cc' : "goodHpHm",
# '[K-]cc' : "goodHpHm",
#}
#
# EndInsertPythonCode
# 
#
# Documentation: Lb0 decaying into L0, K+, K-. Intermediate resonances including N(1710), phi and f'_2(1525). L0 decaying into p+, pi-. 
# EndDocumentation
#
# CPUTime: <1 min
#
# PhysicsWG: BnoC
# Tested: Yes
# Responsible: Guanyue Wan
# Email: gwan@cern.ch
# Date: 20230909
#
Alias      MyLambda     Lambda0
Alias      MyantiLambda anti-Lambda0
ChargeConj MyLambda     MyantiLambda
#
Alias      MyN          N(1710)+
Alias      MyantiN      anti-N(1710)-
ChargeConj MyN          MyantiN
#
Alias      MyPhi     phi
#
Alias      Myff     f'_2
#
Decay Lambda_b0sig
  1.000        K+       K-        MyLambda      PHSP;
  1.000        K-       MyN                     PHSP;
  1.000        MyPhi    MyLambda                PHSP;
  1.000        Myff     MyLambda                PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyN
  1.000        MyLambda      K+     PHSP;
Enddecay
CDecay MyantiN
#
Decay MyLambda
  1.000        p+      pi-      PHSP;
Enddecay
CDecay MyantiLambda
#
Decay MyPhi
  1.000        K+      K-       PHSP;
Enddecay
#
Decay Myff
  1.000        K+      K-       PHSP;
Enddecay
#
End

