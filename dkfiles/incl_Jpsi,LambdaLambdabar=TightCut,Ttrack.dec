# EventType: 24104102
#
# NickName: incl_Jpsi,LambdaLambdabar=TightCut,Ttrack
#
# Descriptor: J/psi(1S) => (Lambda0 => p+ pi-) (Lambda~0 => p~- pi+)
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: J/psi decaying into two Lambda0, and then Lambda0 forced into p pi.
# Lambdas should be in the LHCb acceptance and they should decay in TT.
# EndDocumentation
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# gen = Generation() 
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = 'J/psi(1S) => ^(Lambda0 => ^p+ ^pi-) ^(Lambda~0 => ^p~- ^pi+)'
# tightCut.Cuts      = {
#     '[p+]cc'  : ' goodproton ',
#     '[pi-]cc' : ' goodpion ',
#     'Lambda0' : ' goodLmdTT ',
#     'Lambda~0': ' goodLmdLD '
#     }
# tightCut.Preambulo += [
#     "EVZ       = GFAEVX(GVZ,0)",
#     "inAcc     = in_range ( 0.010 , GTHETA , 0.400 )", 
#     "goodpion  = inAcc & ('pi-' == GABSID)",
#     "goodproton= inAcc & ('p+'  == GABSID)",
#     "goodLmdTT = inAcc & (EVZ > 2500 * mm) & (EVZ < 8000 * mm)",
#     "goodLmdLD = inAcc & (EVZ >    0 * mm) & (EVZ < 2500 * mm)"
#     ]
#
# EndInsertPythonCode
#
# CPUTime: < 8 min
#
# PhysicsWG: Onia 
# Tested: Yes
# Responsible: Ziyi Wang 
# Email: ziyi.wang@cern.ch
# Date: 20230711
#
#
Alias      MyLambda0     Lambda0
Alias      MyAntiLambda0 anti-Lambda0
ChargeConj MyLambda0     MyAntiLambda0
#
Decay MyLambda0
  1.000        p+      pi-                   PHSP;
Enddecay
CDecay MyAntiLambda0
#
Decay J/psisig
  1.000        MyLambda0    MyAntiLambda0      PHSP;
Enddecay
#
End
#
