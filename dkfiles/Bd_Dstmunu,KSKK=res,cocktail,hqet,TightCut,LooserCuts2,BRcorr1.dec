# EventType: 11876133
#
# Descriptor: [B0 -> (D*- -> (anti-D0 -> (K_S0 -> pi+ pi-) K+ K-) pi-) mu+ nu_mu]cc
#
# NickName: Bd_Dstmunu,KSKK=res,cocktail,hqet,TightCut,LooserCuts2,BRcorr1
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = '^[Beauty --> ^(D*(2010)- => ^(D~0 => ^(KS0 => ^pi+ ^pi-) ^K+ ^K-) ^pi-) ^mu+ ... ]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'inAcc             = (in_range (0.005, GTHETA, 0.400))' ,
#     'inEta             = (in_range (1.5, GETA, 5.5))'  ,
#     'goodB             = ((GFAEVX( GVZ, 0) -  GFAPVX( GVZ, 0)) > 1.6 * millimeter)', 
#     'goodD             = (( GP > 10000 * MeV ) & ( GPT > 1800 * MeV ))',
#     'goodKS            = (( GP > 3000 * MeV ) & (GFAEVX(abs(GVZ),0) < 3300 * millimeter))',
#     'goodD0kaons       = (GNINTREE( ("K+" == GABSID) & inAcc & inEta & ( GP > 2000 * MeV ), 4) > 1.5)',
#     'goodKSpions       = (GNINTREE( ("pi+" == GABSID) & inAcc & inEta & ( GP > 1800 * MeV ), 4) > 1.5) ',
#     'GoodSlowPion      = (GNINTREE( ("pi+" == GABSID) & inAcc & inEta & ( GP > 600 * MeV ), 1 ) > 0.5)', 
#     'goodMuon          = (GNINTREE(( "mu+" == GABSID) & ( GP > 4000 * MeV) & ( GPT > 700 * MeV) & inAcc & inEta, 1) > 0.5)',
# ]
# tightCut.Cuts      =    {
#     '[B0]cc'         : 'goodB & goodMuon' ,
#     '[D*(2010)+]cc'  : 'GoodSlowPion' ,
#     '[D0]cc'         : 'goodD  & goodD0kaons' ,
#     '[KS0]cc'        : 'goodKS & goodKSpions',
#     '[mu+]cc'        : 'inAcc', 
#     '[pi+]cc'        : 'inAcc',
#     '[K+]cc'         : 'inAcc',
#     }
#
# EndInsertPythonCode
#
# Documentation: Sum of D* mu nu_mu X and D* tau nu_tau X; D*+ forced into D0 pi+, D0 forced into KSKK with a simple resonant structure, looser tight cuts
# Force the tau- into mu-  nu_tau  anti-nu_mu.
# EndDocumentation 
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Edward Shields
# Email: edward.brendan.shields@cern.ch
# Date: 20200603
# CPUTime: 1min
#
##############
Alias myK_S0  K_S0
ChargeConj myK_S0 myK_S0
#
Alias      Mytau+         tau+
Alias      Mytau-         tau-
ChargeConj Mytau+         Mytau-
#
Alias      MyD0         D0
Alias      MyAntiD0     anti-D0
ChargeConj MyD0         MyAntiD0
#
Alias      MyD*-        D*-
Alias      MyD*+        D*+
ChargeConj MyD*-        MyD*+
#
Alias      MyD_10         D_10
Alias      MyAntiD_10     anti-D_10
ChargeConj MyD_10         MyAntiD_10
#
Alias      MyD_1+         D_1+
Alias      MyD_1-         D_1-
ChargeConj MyD_1+         MyD_1-
#
Alias      MyD_0*+         D_0*+
Alias      MyD_0*-         D_0*-
ChargeConj MyD_0*+         MyD_0*-
#
Alias      MyD_0*0         D_0*0
Alias      MyAntiD_0*0     anti-D_0*0
ChargeConj MyD_0*0         MyAntiD_0*0
#
Alias      MyD'_10         D'_10
Alias      MyAntiD'_10     anti-D'_10
ChargeConj MyD'_10         MyAntiD'_10
#
Alias      MyD'_1+         D'_1+
Alias      MyD'_1-         D'_1-
ChargeConj MyD'_1+         MyD'_1-
#
Alias      MyD_2*+         D_2*+
Alias      MyD_2*-         D_2*-
ChargeConj MyD_2*+         MyD_2*-
#
Alias      MyD_2*0         D_2*0
Alias      MyAntiD_2*0     anti-D_2*0
ChargeConj MyD_2*0         MyAntiD_2*0
#
Decay B0sig 
# FORM FACTORS as per HFAG PDG10
  0.0501   MyD*-        mu+  nu_mu         PHOTOS  HQET 1.20 1.426 0.818 0.908;
  0.0005640  MyD_0*-    mu+  nu_mu         PHOTOS  ISGW2;
  0.0006500  MyD'_1-    mu+  nu_mu         PHOTOS  ISGW2;
  0.0017494  MyD_1-     mu+  nu_mu         PHOTOS  ISGW2;
  0.0006198  MyD_2*-    mu+  nu_mu         PHOTOS  ISGW2;
  0.000462   MyD*-  pi0  mu+  nu_mu        PHOTOS  GOITY_ROBERTS;
  0.000645   MyD*-  pi0 pi0   mu+  nu_mu   PHOTOS  PHSP;
  0.002451   MyD*-  pi+ pi-   mu+  nu_mu   PHOTOS  PHSP;
  0.002604   MyD*-    Mytau+ nu_tau        PHOTOS  ISGW2;
  0.000082   MyD_1-   Mytau+ nu_tau        PHOTOS  ISGW2;
  0.000027   MyD_0*-   Mytau+ nu_tau       PHOTOS  ISGW2;
  0.000056   MyD'_1-   Mytau+ nu_tau       PHOTOS  ISGW2;
  0.000041   MyD_2*-   Mytau+ nu_tau       PHOTOS  ISGW2;
  #
Enddecay
CDecay anti-B0sig
#
SetLineshapePW MyD_1+ MyD*+ pi0 2
SetLineshapePW MyD_1- MyD*- pi0 2
SetLineshapePW MyD_10 MyD*+ pi- 2
SetLineshapePW MyAntiD_10 MyD*- pi+ 2
#
SetLineshapePW MyD_2*+ MyD*+ pi0 2
SetLineshapePW MyD_2*- MyD*- pi0 2
SetLineshapePW MyD_2*0 MyD*+ pi- 2
SetLineshapePW MyAntiD_2*0 MyD*- pi+ 2
#
Decay MyD_0*- 
0.04     MyD*- pi0 pi0                     PHOTOS PHSP;
0.08     MyD*- pi+ pi-                     PHOTOS PHSP;
Enddecay
CDecay MyD_0*+
#
Decay MyAntiD_0*0
0.08    MyD*- pi+ pi0                      PHOTOS PHSP;
Enddecay
CDecay MyD_0*0
#
Decay MyD'_1-
0.250     MyD*- pi0                        PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyD'_1+
#
Decay MyAntiD'_10
0.500    MyD*- pi+                         PHOTOS VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyD'_10
#
Decay MyD_1-
0.200    MyD*- pi0                         PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
0.0208    MyAntiD_0*0 pi-                      PHOTOS PHSP;
0.0156    MyD_0*- pi0                      PHOTOS PHSP;
Enddecay
CDecay MyD_1+
#
Decay MyAntiD_10
0.400    MyD*- pi+                         PHOTOS VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
0.0312    MyD_0*- pi+                      PHOTOS PHSP;
0.0104    MyAntiD_0*0 pi0                      PHSP;
Enddecay
CDecay MyD_10
#
Decay MyD_2*-
0.087    MyD*- pi0                         PHOTOS TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
0.0117    MyAntiD_0*0 pi-                      PHOTOS PHSP;
0.0088    MyD_0*- pi0                      PHOTOS PHSP;
0.004     MyD*- pi0 pi0                    PHOTOS PHSP;
0.008     MyD*- pi+ pi-                    PHOTOS PHSP;
Enddecay
CDecay MyD_2*+
#
Decay MyAntiD_2*0
0.173    MyD*- pi+                         PHOTOS TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
0.0176    MyD_0*- pi+                      PHOTOS PHSP;
0.0059    MyAntiD_0*0 pi0                      PHSP;
0.008     MyD*- pi+ pi0                    PHOTOS PHSP;
Enddecay
CDecay MyD_2*0
#
Decay MyD*-
1.0       MyAntiD0   pi-                   VSS;
Enddecay
CDecay MyD*+
#
# Force Ks -> pi+ pi- to save generating unhelpful events:
Decay myK_S0
1.000     pi+  pi-                      PHSP;
Enddecay
#

ChangeMassMin  a_00  0.987354
ChangeMassMin  a_0+  0.991291
ChangeMassMin  a_0-  0.991291

Decay MyAntiD0
  1.0 myK_S0 K+ K- PTO3P
     MAXPDF   6.0
     # SCANPDF 1000000

     # If you want to add a non-resonant/flat, coherent component, it goes in like this:
     #AMPLITUDE    PHASESPACE
     #COEFFICIENT  POLAR_RAD   0.1    0.0

     # K0bar a0(980)0 -- sub-threshold and has to be NBW else EvtGen crashes
     AMPLITUDE    RESONANCE   BC     a_00
                  ANGULAR     AC
                  TYPE        NBW
     COEFFICIENT  POLAR_RAD   1.0    0.0

     # K0bar phi(1020) -- using relativistic Zemach propagator
     AMPLITUDE    RESONANCE   BC     phi
                  ANGULAR     AC
                  TYPE        RBW_ZEMACH
     COEFFICIENT  POLAR_RAD   0.437  1.91

     # K- a0(980)+ -- sub-threshold and has to be NBW else EvtGen crashes
     AMPLITUDE    RESONANCE   AB     a_0+
                  ANGULAR     CA
                  TYPE        NBW
     COEFFICIENT  POLAR_RAD   0.460  3.59

     # K0bar f0(1400) -- using relativistic Zemach propagator
     AMPLITUDE    RESONANCE   BC     f'_0
                  ANGULAR     AC
                  TYPE        RBW_ZEMACH
     COEFFICIENT  POLAR_RAD   0.435  -2.65
   ;
Enddecay
CDecay MyD0

#
Decay Mytau+
  1.00      mu+  anti-nu_tau  nu_mu        PHOTOS TAULNUNU;
Enddecay
CDecay Mytau-
#
End
