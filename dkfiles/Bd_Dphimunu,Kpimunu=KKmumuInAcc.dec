# EventType: 11676042 
# 
# Descriptor: {[[B0]nos -> (D- -> K+ pi- mu- anti-nu_mu) (phi(1020) -> K+ K-) mu+ nu_mu ]cc, [[B0]os -> (D+ ->  K- pi+ mu+ nu_mu) (phi(1020) -> K+ K-) mu- anti-nu_mu ]cc}
# 
# NickName: Bd_Dphimunu,Kpimunu=KKmumuInAcc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# kkmumuInAcc = Generation().SignalRepeatedHadronization.TightCut
# kkmumuInAcc.Decay = '[^(B0 ==> K+ K- ^mu+ ^mu- nu_mu nu_mu~ {X} {X} {X})]CC'
# kkmumuInAcc.Preambulo += [
#     'inAcc        = (in_range(0.01, GTHETA, 0.400))',
#     'twoKaonsInAcc = (GNINTREE( ("K+"==GID) & inAcc) >= 1) & (GNINTREE( ("K-"==GID) & inAcc) >= 1)'
#     ]
# kkmumuInAcc.Cuts = {
#     '[mu+]cc'  : 'inAcc',
#     '[B0]cc'   : 'twoKaonsInAcc'
#     }
#
# EndInsertPythonCode
#
# Documentation: B0 -> D- phi mu+ nu_mu decays, with KKmumu in acceptance
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: H. Tilquin
# Email: hanae.tilquin@cern.ch
# Date: 20211012
# CPUTime: < 1 min
#
Alias       MyK*0         K*0
Alias       Myanti-K*0    anti-K*0
ChargeConj  MyK*0         Myanti-K*0
#
Alias       MyPhi         phi
ChargeConj  MyPhi         MyPhi
#
Alias       MyD-          D-
Alias       MyD+          D+
ChargeConj  MyD-          MyD+
#
Decay B0sig
  1.000     MyD-     MyPhi    mu+   nu_mu         PHSP;     
Enddecay
CDecay anti-B0sig
#
Decay MyD-
  0.352     MyK*0             mu-   anti-nu_mu    ISGW2;
  0.019     K+       pi-      mu-   anti-nu_mu    PHSP; 
  0.010     K+ pi0   pi-      mu-   anti-nu_mu    PHSP;
Enddecay
CDecay MyD+
#
Decay MyPhi
  1.000     K+       K-                           VSS;
Enddecay
#
Decay MyK*0
  1.000     K+       pi-                          VSS;
Enddecay
CDecay Myanti-K*0
#
End
#
