# EventType: 12113461
#
# Descriptor: [B+ -> (rho+ -> pi+ (pi0 -> gamma gamma)) mu+ mu-]cc
#
# NickName: Bu_rhomumu,pipi=PHSP,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# # 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# # 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = ' Beauty  -> ^mu+ ^mu-  ( rho(770)+ -> ( pi0 -> ^gamma ^gamma ) ^pi+ )'
# tightCut.Cuts      =    {
#     'gamma'     : ' goodGamma ' ,
#     '[mu+]cc'   : ' goodMuon  ' , 
#     '[pi+]cc'   : ' goodPion  ' }
# tightCut.Preambulo += [
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' , 
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' , 
#     'goodMuon  = ( GPT > 500  * MeV ) & ( GP > 2.9 * GeV )     & inAcc ' , 
#     'goodPion  = ( GPT > 100  * MeV ) & ( GP > 1.9 * GeV )     & inAcc ' , 
#     'goodGamma = ( 0 < GPZ ) & ( 150 * MeV < GPT ) & inEcalX & inEcalY ' ]
#
# EndInsertPythonCode
#
# Documentation: Decay file for B+ -> (rho+ -> pi+ (pi0 -> gamma gamma)) mu+ mu- (PHSP) tight cuts
# EndDocumentation
#
# CPUTime: <1 min
#
# PhysicsWG:   RD
# Tested:      Yes
# Responsible: Raul Rabadan
# Email:       raul.iraq.rabadan.trejo@cern.ch
# Date:        20230914
#
Alias      Myrho+      rho+
Alias      Myrho-      rho-
ChargeConj Myrho+      Myrho-
Alias      Mypi0       pi0
#
Decay B+sig
  1.000         Myrho+    mu+     mu-  PHSP;
Enddecay
CDecay B-sig
#
Decay Myrho+
  1.000         pi+       Mypi0        VSS;
Enddecay
CDecay Myrho-
#
Decay Mypi0   
 1.000          gamma     gamma        PHSP;    
Enddecay
#
End
#
