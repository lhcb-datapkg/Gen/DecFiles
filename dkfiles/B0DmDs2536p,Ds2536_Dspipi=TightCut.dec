# EventType: 11198065
# NickName: B0DmDs2536p,Ds2536_Dspipi=TightCut
# Descriptor: [B0 -> (D_s1(2536)+ -> (D_s+ -> K+ K- pi+) pi+ pi-)  (D- -> K+ pi- pi-)]cc
#
# Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '^[B0 -> (D_s1(2536)+ => ^(D_s+ => ^K+ ^K- ^pi+) ^pi+ ^pi-) ^(D- => ^K+ ^pi- ^pi-)]CC'
#tightCut.Preambulo += [
#    'GVZ = LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = (in_range(0.005, GTHETA, 0.400) & in_range ( 1.8 , GETA , 5.2))',
#    'goodB0       = (GP > 25000 * MeV) & (GPT > 1500 * MeV)',
#    'goodD        = (GP > 8000 * MeV) & (GPT > 400 * MeV)',
#    'goodK        = in_range( 1.3 * GeV , GP , 200 * GeV) & (GPT >  80 * MeV)',
#    'goodPi       = in_range( 1.3 * GeV , GP , 200 * GeV) & (GPT >  80 * MeV)',
#]
#tightCut.Cuts = {
#    '[B0]cc'        : 'goodB0',
#    '[D_s+]cc'      : 'goodD',
#    '[D-]cc'        : 'goodD',
#    '[K+]cc'        : 'inAcc & goodK',
#    '[pi+]cc'       : 'inAcc & goodPi'
#    }
#EndInsertPythonCode
#
# Documentation:  B0 flat in Dalitz plot. D_s+ resonant decay forced
#    Decay file for B0 => D- D_s1(2536)+
# EndDocumentation
# CPUTime: < 1 min
# 
# Date:   20240617
# Responsible: Linxuan Zhu
# Email: linxuan.zhu@cern.ch
# PhysicsWG: B2OC
# Tested: Yes

Alias      My_D+   D+
Alias      My_D-   D-
ChargeConj My_D+   My_D- 

Alias       My_D_s-    D_s-
Alias       My_D_s+    D_s+
ChargeConj  My_D_s-    My_D_s+

Alias My_Ds2536          D'_s1+
Alias My_anti-Ds2536     D'_s1-
ChargeConj My_Ds2536     My_anti-Ds2536

#
Decay B0sig
  1.000 My_D- My_Ds2536 PHSP;
Enddecay
CDecay anti-B0sig
#
Decay My_Ds2536
  1.0 My_D_s+ pi+ pi- PHSP;
Enddecay
CDecay My_anti-Ds2536
#
Decay My_D-
  1.0  K+  pi-   pi-  D_DALITZ;
Enddecay
CDecay My_D+
#
Decay My_D_s+
  1.0   K+  K-   pi+   D_DALITZ;
Enddecay
CDecay My_D_s-

End
