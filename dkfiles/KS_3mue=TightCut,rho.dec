# EventType: 34314001
#
# Descriptor: (K_S0 -> mu+ mu- mu+ e-) (K_S0 -> mu+ mu- mu- e+)
#
# NickName: KS_3mue=TightCut,rho
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: (K_S0 -> mu mu mu e) with phase space model and tight generator cut
#  * KS0 endvertex z in [-1m,1m]
#  * KS0 endvertex radial cut at 30mm
# EndDocumentation
#
# CPUTime: < 1 min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[KS0 => mu+ mu- mu+ e-]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, millimeter, GeV" ,
#     "GVX = LoKi.GenVertices.PositionX() " ,
#     "GVY = LoKi.GenVertices.PositionY() " ,
#     "GVZ = LoKi.GenVertices.PositionZ() " ,
#     "vx    = GFAEVX ( GVX, 100 * meter ) " ,    
#     "vy    = GFAEVX ( GVY, 100 * meter ) " ,
#     "rho2  = vx**2 + vy**2 " ,
#     "rhoK  = rho2 < (30 * millimeter )**2 " , 
#     "decay = in_range ( -1 * meter, GFAEVX ( GVZ, 100 * meter ), 1 * meter ) ",
# ]
# tightCut.Cuts      =    {
#     'KS0'  : ' decay & rhoK',
#                         }
# EndInsertPythonCode
#
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Miguel Ramos Pernas, Adrian Casais Vidal
# Email: Miguel.Ramos.Pernas@cern.ch, Adrian.Casais.Vidal@cern.ch
# Date: 20181011
#
Decay K_S0sig
  0.5       mu+ mu- mu+ e-    PHSP;
  0.5       mu+ mu- mu- e+    PHSP;
Enddecay
#
End
