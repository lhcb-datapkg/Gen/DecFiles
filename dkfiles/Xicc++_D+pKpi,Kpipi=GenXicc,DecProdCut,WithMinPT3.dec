# EventType:  26166051
#
# Descriptor: [Xi_cc++ -> (D+ => pi+ pi+ K-) p+ K- pi+]cc
#
# NickName: Xicc++_D+pKpi,Kpipi=GenXicc,DecProdCut,WithMinPT3
#
# Production: GenXicc
#
# Cuts: XiccDaughtersInLHCbAndWithMinPT
#
# CutsOptions: MinXiccPT 3000*MeV
#
# CPUTime: < 1 min
#
# Documentation: Xicc++ decay to D+ p+ K- pi+ by phase space model, with the D+ decaying to pi+ pi+ K-.
# All daughters of Xicc++ are required to be in the acceptance of LHCb 
# and the Xicc++ PT is required to be larger than 3000 MeV/c. 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Murdo Traill
# Email: murdo.thomas.traill@cern.ch
# Date: 20170129
#
#Alias	   Xi_cc++sig        Xi_cc++
#Alias	   anti-Xi_cc--sig   anti-Xi_cc--
#ChargeConj Xi_cc++sig		 anti-Xi_cc--sig
#
Alias      MyD+          D+
Alias      MyD-          D-
ChargeConj MyD+			 MyD-
#
Decay Xi_cc++sig
  1.000    MyD+   p+  K-	   pi+       PHSP;
Enddecay
CDecay anti-Xi_cc--sig
#
Decay MyD+
  1.000        K-        pi+        pi+             PHSP;
Enddecay
CDecay MyD-
#
End
#
