# EventType: 12365403
#
# Descriptor: [B+ -> (D*- -> (anti-D0 -> pi+ pi-) pi-) pi+ pi+ pi0]cc
#
# NickName: Bu_DstX,cocktail,D0pi,pipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: secondary D*+ -> D0 (-> pi- pi+) decays with B- parent, tight cuts to match Run2 HLT requirements of prompt D02HH
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# signal     = Generation().SignalRepeatedHadronization
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '[ [B+]cc --> ^(D*(2010)- => ^( D~0 => ^pi+ ^pi- ) pi-) ... ]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import micrometer, MeV, GeV                    ',
#     'from LoKiCore.math import atan2                                               ',
#     'inAccLoose   =  in_range ( 0.012 , GTHETA , 0.35 ) & in_range ( -0.3  , atan2(GPX , GPZ), 0.3  ) & in_range ( -0.26 , atan2(GPY , GPZ), 0.26 ) ',
#     'inAcc        =  in_range ( 0.014 , GTHETA , 0.30 ) & in_range ( -0.28 , atan2(GPX , GPZ), 0.28 ) & in_range ( -0.24 , atan2(GPY , GPZ), 0.24 ) ',
#     'fastTrack    =  ( GPT > 775 * MeV ) & ( GP  > 4.85 * GeV )                    ',
#     'goodTrack    =  inAccLoose & fastTrack                                        ',
#     'goodD0       =  inAcc & ( GPT > 1.94 * GeV )                                  ',
#     'goodSlowPion =  ( GPT > 190 * MeV ) & ( GP > 0.97 * GeV ) & inAcc             ',
#     'goodDst      =  GCHILDCUT ( goodSlowPion , "[ D*(2010)+ =>  Charm ^pi+ ]CC" ) '
# ]
# tightCut.Cuts     =    {
#     '[D*(2010)-]cc' : 'goodDst   ',
#     '[D~0]cc'       : 'goodD0    ',
#     '[pi+]cc'       : 'goodTrack '
#     }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Tommaso Pajero
# Email: tommaso.pajero@cern.ch
# Date: 20200129
# CPUTime: < 1 min
#
Alias      MyD*+    D*+
Alias      MyD*-    D*-
ChargeConj MyD*+    MyD*-
#
Alias      MyD0     D0
Alias      MyantiD0 anti-D0
ChargeConj MyD0     MyantiD0
#
Decay B+sig
    1.5       MyD*-   pi+         pi+     pi0             PHSP;
    0.92      MyD*+   anti-D*0    K0                      PHSP;
    0.38      MyD*+   anti-D0     K0                      PHSP;
    0.26      MyD*-   pi+         pi+     pi+     pi-     PHSP;
    0.135     MyD*-   pi+         pi+                     PHSP;
    0.132     MyD*-   D*+         K+                      PHSP;
    0.132     D*-     MyD*+       K+                      PHSP;
    0.081     MyD*+   anti-D*0                            SVV_HELAMP 0.56 0.0 0.96 0.0 0.47 0.0;
    0.063     MyD*+   D-          K+                      PHSP;
    0.060     MyD*-   D+          K+                      PHSP;
    0.039     MyD*+   anti-D0                             SVS;
Enddecay
CDecay B-sig
#
Decay MyD*-
  1.0   MyantiD0    pi-     VSS;
Enddecay
CDecay MyD*+
#
Decay MyantiD0
  1.0   pi+         pi-    PHSP;
Enddecay
CDecay MyD0
#
End
