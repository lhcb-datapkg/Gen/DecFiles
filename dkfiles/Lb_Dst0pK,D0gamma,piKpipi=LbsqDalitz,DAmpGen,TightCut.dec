# EventType: 15166278
#
# Descriptor: [Lambda_b0 => (D*(2007)0 -> (D0 -> K+ pi- pi+ pi-) gamma) p+ K-]cc
#
# NickName: Lb_Dst0pK,D0gamma,piKpipi=LbsqDalitz,DAmpGen,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut 
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalPlain.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalPlain.TightCut
#tightCut.Decay = '^[Beauty => (D*(2007)0 -> ^(D0 => ^K+ ^pi- ^pi+ ^pi-) gamma) ^p+ ^K-]CC'
#tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = in_range(0.005, GTHETA, 0.400) & in_range(1.8, GETA, 5.2)',
#    'inY          = in_range(1.8, GY, 4.8)',
#    'goodH        = (GP > 1000 * MeV) & (GPT > 98 * MeV) & inAcc',
#    'goodB0       = (GP > 25000 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.05 * millimeter) & inY',
#    'goodD0       = (GP > 10000 * MeV) & (GPT > 500 * MeV) & inY',
#    'goodBDaugK   = (GNINTREE( ("K+" == GABSID) & (GP > 2000 * MeV), 1) > 0.5)',
#    'goodBDaugP   = (GNINTREE( ("p+" == GABSID) & (GP > 2000 * MeV), 1) > 0.5)'
#]
#tightCut.Cuts = {
#    '[pi+]cc'         : 'goodH',
#    '[K+]cc'          : 'goodH',
#    '[p+]cc'          : 'goodH',
#    'Beauty'          : 'goodB0 & goodBDaugK & goodBDaugP', 
#    '[D0]cc'          : 'goodD0'
#    }
#EndInsertPythonCode
#
# Documentation: Lb decay with flat square Dalitz model, Dst0 forced to D0 gamma, D0 decay following AmpGen LHCb model DtopiKpipi_v2, tight cuts 
# EndDocumentation
#
# CPUTime: <2min
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Yuya Shimizu 
# Email: yuya.shimizu@cern.ch 
# Date: 20201206
#

Alias MyD*0       D*0
Alias Myanti-D*0  anti-D*0
Alias MyD0        D0
Alias Myanti-D0   anti-D0
ChargeConj MyD*0  Myanti-D*0
ChargeConj MyD0   Myanti-D0

Decay Lambda_b0sig
  1.0   MyD*0       p+         K-   FLATSQDALITZ;
Enddecay
CDecay anti-Lambda_b0sig

Decay MyD*0
  1.0   MyD0        gamma           VSP_PWAVE;
Enddecay
CDecay Myanti-D*0

Decay MyD0
  1.0  K+    pi-    pi+    pi-      LbAmpGen DtopiKpipi_v2;
Enddecay
CDecay Myanti-D0

End
