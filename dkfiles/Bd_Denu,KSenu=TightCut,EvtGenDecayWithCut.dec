# EventType: 11584100
#
# Descriptor: [B0 -> (D- -> (KS0 -> pi+ pi-) e- anti-nu_e) e+ nu_e]cc
# NickName: Bd_Denu,KSenu=TightCut,EvtGenDecayWithCut
#
# Documentation: D chain background for B0 -> KSee, with decay products in acceptance
# EndDocumentation
# 
# PhysicsWG: RD
# Tested: Yes
# Responsible: John Smeaton
# Email: john.smeaton@cern.ch
# Date: 20210329
# CPUTime: <1min
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool( LoKi__GenCutTool,'HighVisMass')
# evtgendecay.HighVisMass.Decay = '^[ Beauty => ( D+ => ( KS0 => pi+ pi- ) e+ nu_e ) e- nu_e~ ]CC'
# evtgendecay.HighVisMass.Cuts = {
#     'Beauty': " massCut ",
#     '[KS0]cc'    : "inAcc & ( GPT > 200 * MeV )",
#     '[e-]cc'     : "inAcc & ( GPT > 200 * MeV )",
# }
# evtgendecay.HighVisMass.Preambulo += [
#     "massCut = GMASS('e-'==GID,'e+'==GID,'KS0'==GABSID) > 4400 * MeV",
#     "inAcc   = in_range ( 0.005 , GTHETA , 0.400 ) " 
# ]
#
# gen.SignalRepeatedHadronization.addTool(LoKi__GenCutTool ,'TightCut')
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay   = '[ Beauty => ( D+ => ^( KS0 => pi+ pi- ) ^e+ nu_e ) ^e- nu_e~ ]CC'
# tightCut.Cuts    = {
#     '[KS0]cc'    : "inAcc & ( GPT > 200 * MeV )",
#     '[e-]cc'     : "inAcc & ( GPT > 200 * MeV )",
# }
# tightCut.Preambulo += [
#     "inAcc   = in_range ( 0.005 , GTHETA , 0.400 ) " 
# ]
# EndInsertPythonCode
#
#
Alias		MyD-	D-
Alias 		MyD+	D+
ChargeConj	MyD-	MyD+
#
Alias		MyKS	K_S0
ChargeConj	MyKS	MyKS
#
Decay B0sig
1.000 		MyD- e+ nu_e		PHOTOS HQET2 1.185 1.081;
Enddecay
CDecay anti-B0sig
#
Decay MyD-
1.000		MyKS e- anti-nu_e	PHOTOS ISGW2;
Enddecay
CDecay MyD+
#
Decay MyKS
1.000		pi+ pi-			PHSP;
Enddecay
#
End
