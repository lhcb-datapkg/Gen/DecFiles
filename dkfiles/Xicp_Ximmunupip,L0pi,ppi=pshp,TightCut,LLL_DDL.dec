# EventType: 26615191 
#
# Descriptor: [Xi_c+ -> (Xi*0 -> (Xi- -> (Lambda0 -> p+ pi-) pi-) pi+) mu+ nu_mu]cc 
#
# NickName: Xicp_Ximmunupip,L0pi,ppi=pshp,TightCut,LLL_DDL
# Cuts: LoKi::GenCutTool/TightCut
# FullEventCuts: LoKi::FullGenEventCut/GenEvtCut
# ExtraOptions: SwitchOffAllPythiaProcesses
# InsertPythonCode:
# from Configurables import (Pythia8Production, ToolSvc, EvtGenDecayWithCutTool, LoKi__GenCutTool, LoKi__FullGenEventCut)
# Generation().SignalPlain.addTool(Pythia8Production, name="Pythia8Production")
# Generation().SignalPlain.Pythia8Production.Commands += ["SoftQCD:all=off","HardQCD:hardccbar=on"]
# Generation().SignalPlain.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool( EvtGenDecayWithCutTool )
# EvtGenCut = ToolSvc().EvtGenDecayWithCutTool
# EvtGenCut.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# EvtGenCut.CutTool = "LoKi::GenCutTool/HyperonDTCut"
# EvtGenCut.addTool(LoKi__GenCutTool,"HyperonDTCut")
# EvtGenCut.HyperonDTCut.Decay = "[Xi_c+ -> ^(Xi- -> ^(Lambda0 -> p+ pi-) pi-) mu+ nu_mu pi+]CC"
# EvtGenCut.HyperonDTCut.Preambulo += [
#   "from GaudiKernel.PhysicalConstants import c_light",
#   "from GaudiKernel.SystemOfUnits import mm, ns"
#   ]
# EvtGenCut.HyperonDTCut.Cuts = {
#   '[Xi-]cc'       : "(GCTAU>0.0019*ns*c_light) & (GCTAU<50*mm)",
#   '[Lambda0]cc'   : "(GCTAU>0.0044*ns*c_light) & (GCTAU<200*mm)"
# }
# #
# Generation().SignalPlain.addTool(LoKi__GenCutTool,'TightCut')
# SigCut = Generation().SignalPlain.TightCut
# SigCut.Decay = "[^(Xi_c+ ==> ^(Xi- => ^(Lambda0 => ^p+ ^pi-) ^pi-) ^mu+ nu_mu pi+)]CC"
# SigCut.Filter = True
# SigCut.Preambulo += [
#   "from LoKiCore.functions import in_range"  ,
#   "from GaudiKernel.SystemOfUnits import GeV, MeV, mrad, mm",
#   "inAcc    = in_range ( 1.80 , GETA , 5.10 ) " ,
#   "EVZ   = GFAEVX(GVZ,0)",
#   "OVZ   = GFAPVX(GVZ,0)"
#  ]
# SigCut.Cuts = {
#   '[Xi_c+]cc'   : "(GPT>1.0*GeV) & (EVZ-OVZ>0.2*mm) & (GCHILD(EVZ, (GABSID=='Xi-'))-EVZ>1.8*mm) & inAcc",
#   '[Xi-]cc'     : "(GPT>350*MeV) & inAcc",
#   '[Lambda0]cc' : "(GPT>300*MeV) & inAcc",
#   '[mu+]cc'     : "(GPT>150*MeV) & inAcc",
#   '[pi-]cc'     : "(GPT>110*MeV) & inAcc",
#   '[p+]cc'      : "(GPT>50*MeV) & inAcc"
# }
# #
# Generation().addTool(LoKi__FullGenEventCut,'GenEvtCut')
# EvtCut = Generation().GenEvtCut
# EvtCut.Preambulo += [
#   "from GaudiKernel.SystemOfUnits import mm",
#   "EVZ     = GFAEVX(GVZ,0)",
#   "EVR     = GFAEVX(GVRHO,0)",
#   "OVZ     = GFAPVX(GVZ,0)",
#   "goodXic = GSIGNALINLABFRAME & (GABSID=='Xi_c+') & (GCHILDCUT((EVR<42*mm) & (EVZ<666*mm), '[Xi_c+ ==> ^Xi- mu+ nu_mu pi+]CC'))"\
#            " & (GCHILDCUT((EVZ<2500*mm), '[Xi_c+ ==> (Xi- => ^Lambda0 pi-) mu+ nu_mu pi+]CC'))"
#  ]
# EvtCut.Code = "has(goodXic)"
# EndInsertPythonCode
#
# CPUTime: ChronoStatSvc 241  [s] 
#
# Documentation: Xi_c+ decay to Xi- mu+ nu_mu pi+ by phase space model (50% go via Xi*0 resonance). Tight generator level cuts. Cut efficiency 5.5556 +/- 2.2041 %
# EndDocumentation
#
#
# PhysicsWG: Charm 
# Tested: Yes
# Responsible: Miroslav Saur, Ziyi Wang, Patrik Adlarson, Marian Stahl
# Email: ziyiw@cern.ch, patrik.harri.adlarson@cern.ch
# Date: 20241010
#
#
Alias      MyL0         Lambda0
Alias      MyantiL0     anti-Lambda0
ChargeConj MyL0         MyantiL0
#
Alias      MyXi-        Xi-
Alias      Myanti-Xi+   anti-Xi+
ChargeConj MyXi-        Myanti-Xi+
#
Alias      MyXist0        Xi*0
Alias      Myanti-Xist0   anti-Xi*0
ChargeConj MyXist0        Myanti-Xist0
#
Decay Xi_c+sig
  0.500 MyXi-   mu+   nu_mu   pi+ PHSP;
  0.500 MyXist0   mu+   nu_mu   PHSP;
Enddecay
CDecay anti-Xi_c-sig
#
Decay MyXist0
  1.000 MyXi- pi+  PHSP;
Enddecay
CDecay Myanti-Xist0
#
Decay MyXi-
  1.000 MyL0 pi-  PHSP;
Enddecay
CDecay Myanti-Xi+
#
Decay MyL0
  1.000        p+      pi-      PHSP;
Enddecay
CDecay MyantiL0
#
End
#
