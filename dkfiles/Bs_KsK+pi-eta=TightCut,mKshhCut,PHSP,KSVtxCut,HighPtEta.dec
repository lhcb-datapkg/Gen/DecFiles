# EventType: 13104523
#
# Descriptor: [Beauty -> K+ pi- (KS0 -> pi+ pi-) (eta -> gamma gamma)]cc
#
# NickName: Bs_KsK+pi-eta=TightCut,mKshhCut,PHSP,KSVtxCut,HighPtEta
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation() 
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/mKshhCut"
# evtgendecay.addTool( LoKi__GenCutTool ,'mKshhCut')
# evtgendecay.mKshhCut.Decay = '[^(Beauty => K+ pi- KS0 eta)]CC'
# evtgendecay.mKshhCut.Cuts  = {'[B_s0]cc' : ' mKshhCut '}
# evtgendecay.mKshhCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "mKshhCut   = ( GMASS(CS('[(Beauty => ^K+ pi- KS0 eta)]CC'),CS('[(Beauty => K+ ^pi- KS0 eta)]CC'), CS('[(Beauty => K+ pi- ^KS0 eta)]CC')) < 2 * GeV)"]
#
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[Beauty => ^K+ ^pi- ^(KS0 => ^pi+ ^pi-) ^eta]CC'
# tightCut.Cuts      =    {
#     '[pi+]cc'        : 'inAcc' , 
#     '[K+]cc'         : 'inAcc' , 
#     'KS0'            : 'decayBeforeTT',
#     'eta'            : 'goodEta'
#     }
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inEcalX       =   abs ( GPX / GPZ ) < 4.5  / 12.5",
#     "inEcalY       =   abs ( GPY / GPZ ) < 3.5  / 12.5",
#     "inEcalHole    = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 )",
#     "InEcal        = inEcalX & inEcalY & ~inEcalHole ",
#     "inAcc         = in_range ( 0.005 , GTHETA , 0.400 ) " , 
#     "goodEta       = ('eta' == GABSID) & ( GPT > 2.5 * GeV ) & InEcal",
#     "inAcc         = in_range ( 0.005 , GTHETA , 0.400 ) ",
#     "decayBeforeTT = GVEV & ( GFAEVX ( GVZ , 1.e+10 ) < 2400 * millimeter)"
#     ]  
#
# EndInsertPythonCode
#
# Documentation: Bkgd for KsKpigamma, all in PHSP, Kshh in acceptance, mKshh < 2 GeV, high PT eta in Acc, KS0 VTZ < 2.4 m
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20211119
#
Alias      MyK0s  K_S0
ChargeConj MyK0s  MyK0s
#
Alias      Myeta        eta
ChargeConj Myeta        Myeta
#
Decay B_s0sig
  1.000   K+  pi-    MyK0s      Myeta         PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyK0s
  1.000   pi+         pi-       PHSP;
Enddecay
#
Decay Myeta
  1.000        gamma      gamma           PHSP;
Enddecay
#
End
