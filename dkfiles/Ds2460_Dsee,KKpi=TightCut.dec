# EventType: 27185081
#
# Descriptor: [D_s1(2460)+ -> (D_s+ -> K+ K- pi+) e+ e- ]cc
#
# ParticleValue: "D_s1(2460)+           172       20433   1.0      2.45950000      6.582100e-22                     D_s1+       20433      0.005", " D_s1(2460)-           176      -20433  -1.0      2.45950000      6.582100e-22                     D_s1-      -20433      0.005"
#
# NickName: Ds2460_Dsee,KKpi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# # 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# # 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[ D_s1(2460)+ => (D_s+ => ^K+ ^K- ^pi+ ) ^e+ ^e- ]CC'
# tightCut.Cuts      =    {
#     '[K+]cc'         : ' goodKaon ' , 
#     '[pi+]cc'        : ' goodPion ' , 
#     '[e+]cc'        : ' goodElec ' } 
#
# tightCut.Preambulo += [
#     'inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     'goodKaon   = ( GPT > 0.25 * GeV ) & ( GP > 1.9 * GeV ) & inAcc ' , 
#     'goodPion   = ( GPT > 0.25 * GeV ) & ( GP > 1.9 * GeV ) & inAcc ' , 
#     'goodElec   = inAcc' ] 
#
# EndInsertPythonCode
#
# Documentation: Search for excited Ds
# Mass of Ds1(2460) taken from PDG2024. Width set to 1 MeV based on the current UL
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: 4 min
# Responsible:    Marco Pappagallo
# Email: marco.pappagallo@cern.ch
# Date: 20250122
#
Alias MyD_s- D_s-
Alias MyD_s+ D_s+
ChargeConj MyD_s+ MyD_s-
#
Decay D_s1+sig
1.0000     MyD_s+    e+   e-      PHSP;
Enddecay
CDecay D_s1-sig
#
Decay MyD_s+
  1.000        K-        K+        pi+             D_DALITZ;
Enddecay
CDecay MyD_s-
#
End
