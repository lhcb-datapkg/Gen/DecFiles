# EventType: 16155132
#
# Descriptor: [Xi_b- -> (Omega- -> (Lambda0 -> p+ pi-) K-) (J/psi(1S) -> e+ e-)]cc
#
# NickName: Omegab_JpsiOmega,ee,LambdaK=phsp,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# ParticleValue: "Xi_b- 122 5132 -1.0 6.046 1.64e-012 Xi_b- 5132 0.000000e+000", "Xi_b~+ 123 -5132 1.0 6.046 1.64e-012 anti-Xi_b+ -5132 0.000000e+000"
#
# Documentation: Omega forced to Lambda K , Lambda forced to p pi.
# Tight generator cuts on the Omega and its decay products.
# Omega_b difficult to produce with Pythia (very low fragmentation prob) so alter Xib instead.
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[(Xi_b- => ^(Omega- => ^(Lambda0 => ^p+ ^pi-) ^K-) (J/psi(1S) => ^e+ ^e-))]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, GeV" ,
#     "inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )     " ,
#     "inAccH       =  in_range ( 0.001 , GTHETA , 0.390 )     " ,
#     "inEtaL       =  in_range ( 1.5  , GETA   , 5.5 )        " , 
#     "inEtaLD      =  in_range ( 1.5  , GETA   , 7.5 )        " ,
#     "inP_pi       =  ( GP > 1.5  *  GeV ) ",
#     "inP_K        =  ( GP > 1.  *  GeV ) ",
#     "inP_p        =  ( GP > 1.5 *  GeV ) ",
#     "inP_el       =  ( GP > 2.  *  GeV ) ",
#     "goodElectron =  inAcc  & inP_el & inEtaL " ,
#     "goodPion     =  inAccH & inP_pi & inEtaLD" ,
#     "goodKaon     =  inAccH & inP_K & inEtaLD" ,
#     "goodProton   =  inAccH & inP_p  & inEtaLD" ,
#     "GVZ = LoKi.GenVertices.PositionZ()       " ,
#     "decay = in_range ( -1.1 * meter, GFAEVX ( GVZ, 100 * meter ), 3 * meter )",
# ]
# tightCut.Cuts      =    {
#     "[Omega-]cc"   : "decay", 
#     "[Lambda0]cc"  : "decay",
#     "[pi-]cc"      : "goodPion" , 
#     "[K-]cc"       : "goodKaon",
#     "[p+]cc"       : "goodProton",
#     "[e+]cc"       : "goodElectron"
#                         }
# EndInsertPythonCode
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Niladri Sahoo
# Email: Niladri.Sahoo@cern.ch
# Date: 20211025
# CPUTime: < 1 min
#
Alias      MyOmega     Omega-
Alias      Myanti-Omega anti-Omega+
ChargeConj Myanti-Omega MyOmega
#
Alias      MyLambda      Lambda0
Alias      Myanti-Lambda anti-Lambda0
ChargeConj Myanti-Lambda MyLambda
# 
Alias      MyJ/psi       J/psi
ChargeConj MyJ/psi       MyJ/psi
# 
Decay Xi_b-sig 
  1.000    MyOmega          MyJ/psi      PHSP;
Enddecay
CDecay anti-Xi_b+sig
#
Decay MyJ/psi
  1.000     e+  e-                       PHOTOS VLL;
Enddecay
#
Decay MyOmega
  1.000     MyLambda   K-                HELAMP   0.713  0.0  0.702  0.0;
Enddecay
CDecay Myanti-Omega
#
Decay MyLambda
  1.000     p+   pi-                     HELAMP   0.936   0.0   0.351   0.0;
Enddecay
CDecay Myanti-Lambda
#
End
