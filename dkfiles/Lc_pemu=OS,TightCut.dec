# EventType: 25313011
# 
# Descriptor: [Lambda_c+ -> p+ e+ mu-]cc
# 
# NickName: Lc_pemu=OS,TightCut
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Lambda_c -> p emu LFV, PHSP MODEL, 
# GL cuts, double arrows in Decay
# EndDocumentation
#
# 
# PhysicsWG:   Charm
# Tested:      Yes
# Responsible: Marcin Chrzaszcz, Jolanta Brodzicka
# Email:       mchrzasz@cern.ch, jolanta.brodzicka@cern.ch
# Date:        20190208
#
# CPUTime: <1min
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[ Lambda_c+ =>  ^p+ ^(mu+|e+) ^(mu-|e-) ]CC'
# tightCut.Cuts      =    {
#     '[e+]cc'        : ' goodElectron & inAcc & inCaloAcc ' , 
#     '[mu+]cc'       : ' goodMuon & inAcc   ' , 
#     '[p+]cc'        : ' goodProton & inAcc ' , 
#     '[Lambda_c+]cc' : ' goodLambdac & ~GHAS (GBEAUTY, HepMC.ancestors)' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter, micrometer',
#     'inAcc      = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     'inCaloAcc   = ( in_range(0.000, abs(GPX/GPZ), 0.300) & in_range(0.000, abs(GPY/GPZ), 0.250) & (GPZ > 0) )',
#     'goodProton = ( GPT > 0.2 * GeV ) & ( GP > 2. * GeV ) ' , 
#     'goodElectron = ( GPT > 0.2 * GeV ) & ( GP > 2. * GeV )',
#     'goodMuon   = ( GPT > 0.2 * GeV ) & ( GP > 2. * GeV ) ' , 
#     'goodLambdac  = ( GTIME > 50 * micrometer ) ' ]
#
# EndInsertPythonCode


Decay Lambda_c+sig
  0.50000         p+      e-      mu+     PHSP;
  0.50000         p+      e+      mu-     PHSP;
Enddecay
CDecay anti-Lambda_c-sig
 
#
End
