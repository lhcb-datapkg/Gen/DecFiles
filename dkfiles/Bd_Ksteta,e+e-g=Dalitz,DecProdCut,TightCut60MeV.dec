# EventType: 11124411
# 
# Descriptor: [B0 -> (eta -> e+ e- gamma) (K*0 -> K+ pi- )]cc 
# 
# NickName: Bd_Ksteta,e+e-g=Dalitz,DecProdCut,TightCut60MeV
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Eta decays to e+e-gamma, Kst to K+ pi-
# EndDocumentation
#
# CPUTime: < 1 min
# PhysicsWG: RD
# Tested: Yes
# Responsible: Fabrice Desse
# Email: fadesse@cern.ch
# Date: 20190214
#
# InsertPythonCode:
# #
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/HighVisMass"
# evtgendecay.addTool(LoKi__GenCutTool ,'HighVisMass')
# evtgendecay.HighVisMass.Decay = '[^(B0 => (K*(892)0 => K+ pi- ) (eta => e+ e- gamma) )]CC'
# evtgendecay.HighVisMass.Cuts  = { '[B0]cc' : "visMass" }
# evtgendecay.HighVisMass.Preambulo += ["visMass = ( ( GMASS ( 'e+' == GID , 'e-' == GID ) ) < 60 * MeV )" ]
#
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay   = '[(B0 => (K*(892)0 => ^K+ ^pi- ) (eta => ^e+ ^e- gamma))]CC'
# tightCut.Cuts    =    {
#     '[K+]cc'     : "inAcc",
#     '[pi-]cc'    : "inAcc",
#     '[e+]cc'     : "inAcc",
#     '[e-]cc'     : "inAcc"}
# tightCut.Preambulo += [
#     "inAcc   = ( 240 * MeV  < GPT )   & in_range (   1.85 , GETA ,  5.05 )"]
# EndInsertPythonCode
#
Alias      MyK*0       K*0
Alias      Myanti-K*0  anti-K*0
ChargeConj MyK*0       Myanti-K*0
Alias      MyEta       eta
ChargeConj MyEta       MyEta
#
Decay B0sig
  1.000         MyK*0     MyEta        SVS;
Enddecay
CDecay anti-B0sig
#
Decay MyK*0
  1.000        K+       pi-              VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyEta
  1.000         e+   e-   gamma        PHSP;
Enddecay
#
End

