# EventType: 15574032
#
# Descriptor: [Lambda_b0 -> (D0 -> K- mu+ nu_mu) mu- anti-nu_mu p+]cc
#
# NickName: Lb_D0pmunu,Kmunu=VisibleInAcceptance,HighVisMass
#
# Cuts: LoKi::GenCutTool/HighVisMass
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool(LoKi__GenCutTool ,'HighVisMass')
# #
# tightCut = gen.SignalPlain.HighVisMass
# tightCut.Decay  = '[^(Lambda_b0 => (D0 => ^K- ^mu+ nu_mu) ^mu- nu_mu~ ^p+)]CC'
# tightCut.Cuts   =   {
#     '[K-]cc'      : "inAcc",
#     '[p+]cc'      : "inAcc",
#     '[mu+]cc'     : "inAcc",
#     '[mu-]cc'     : "inAcc",
#     '[Lambda_b0]cc'     : "visMass" }
# tightCut.Preambulo += [
#     "inAcc   = in_range ( 0.005 , GTHETA , 0.400 ) " ,
#     "visMass  = ( ( GMASS ( 'mu+' == GID , 'mu-' == GID, 'K-' == GABSID, 'p+' == GABSID ) ) > 4500 * MeV ) " ]
# EndInsertPythonCode
#
# Documentation: Lambda_b0 -> D0 p mu nu sample for double-semileptonic
# background studies in Lambda_b0 -> p K mu mu, required to have the
# visible mass larger than 4.5 GeV
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Vitalii Lisovskyi
# Email: vitalii.lisovskyi@cern.ch
# Date: 20170615
# CPUTime: 6 min
#
Alias      MyD0       D0
Alias      MyD0bar    anti-D0
ChargeConj MyD0       MyD0bar
#
Decay Lambda_b0sig
1.000     MyD0 mu- anti-nu_mu p+  PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyD0
1.000     K- mu+ nu_mu           ISGW2;  
Enddecay
CDecay MyD0bar
#
End
#

