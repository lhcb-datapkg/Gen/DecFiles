# EventType: 11246030
#
# Descriptor: [B0 -> (K*0 -> K+ pi-) (psi(2S) -> (J/psi -> mu+ mu-) pi+ pi-)]cc
#
# NickName: Bd_ccKst,Jpsipipi,mm=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: B0 decay to { X(3872) | psi(2S) } [J/psi pi+pi-] K+ pi- (90% K*(892), 10% phsp), no CPV, tight cuts
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal     = generation.SignalRepeatedHadronization
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '^[Beauty ==> ( (X_1(3872) | psi(2S) ) ==> ^( J/psi(1S) => ^mu+ ^mu- ) ^pi+ ^pi- ) ^K+ ^pi- ]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import micrometer, millimeter, MeV, GeV ',
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         ' ,
#     'inEta        =  in_range ( 1.95  , GETA   , 5.050 )         ' ,
#     'inY          =  in_range ( 1.9   , GY     , 4.6   )         ' ,
#     'fastTrack    =  ( GPT > 180 * MeV ) & ( GP  > 3.0 * GeV )   ' ,
#     'goodTrack    =  inAcc & inEta & fastTrack                   ' ,
#     'goodPsi      =  inY                                         ' ,
#     'longLived    =  75 * micrometer < GTIME                     ' ,
#     'goodB        =  inY & longLived                             ' 
# ]
# tightCut.Cuts     =    {
#     '[B0]cc'         : 'goodB     ' ,
#     'J/psi(1S)'      : 'goodPsi   ' ,
#     '[K+]cc'         : 'goodTrack ' ,
#     '[pi+]cc'        : 'goodTrack ' ,
#     '[mu+]cc'        : 'goodTrack & ( GPT > 500 * MeV ) '
#     }
#
# # Generator efficiency histos:
# tightCut.XAxis = ( "GPT/GeV" , 1.0 , 20.0 , 38  )
# tightCut.YAxis = ( "GY     " , 2.0 ,  4.5 , 10  )
#
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Pavel Krokovny
# Email: pavel.krokovny@cern.ch
# Date: 20200310
# CPUTime: < 1 min
#
Define Hp 0.159
Define Hz 0.775
Define Hm 0.612
Define pHp 1.563
Define pHz 0.0
Define pHm 2.712
#
Alias      MyX_1(3872)  X_1(3872)
ChargeConj MyX_1(3872)  MyX_1(3872)
Alias      Mypsi2s      psi(2S)
ChargeConj Mypsi2s      Mypsi2s
Alias      MyJ/psi       J/psi
ChargeConj MyJ/psi       MyJ/psi
Alias      MyK*0        K*0
Alias      Myanti-K*0   anti-K*0
ChargeConj MyK*0        Myanti-K*0
Alias      Myrho0       rho0
ChargeConj Myrho0       Myrho0
#
Decay B0sig
  0.450     MyX_1(3872)   MyK*0          SVV_HELAMP Hp pHp Hz pHz Hm pHm;
  0.050     MyX_1(3872)   K+   pi-       PHOTOS PHSP;
  0.450         Mypsi2s   MyK*0          SVV_HELAMP Hp pHp Hz pHz Hm pHm;
  0.050		Mypsi2s   K+   pi-       PHOTOS PHSP;
Enddecay
CDecay anti-B0sig
#
Decay Mypsi2s
  1.000  MyJ/psi        pi+   pi-         PHOTOS VVPIPI;
Enddecay
#
Decay MyX_1(3872)
  1.000     MyJ/psi        Myrho0         HELAMP 0.707107 0  0.707107 0  0.707107 0  0 0  -0.707107 0  -0.707107 0  -0.707107 0;
Enddecay
#
Decay MyJ/psi
  1.000         mu+         mu-          PHOTOS VLL;
Enddecay
#
Decay MyK*0
  1.000         K+        pi-            PHOTOS VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Myrho0
  1.000         pi+     pi-              PHOTOS VSS;
Enddecay
#
End
