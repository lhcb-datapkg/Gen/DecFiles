# EventType: 14243211
#
# Descriptor: [B_c+ -> ( [chi_c1(1P) , chi_c2(1P)]  -> (J/psi(1S) -> mu+ mu- )  gamma ) pi+ ]cc
#
# NickName: Bc_chicpi,jpsig,mm=BcVegPy,TightCut
#
# Production: BcVegPy
#
# Cuts: LoKi::GenCutToolWithDecay/TightCut
#
# Documentation: The Bc decay into chic pi final state with chic -> J/psi gamma. The cocktail of chi_c1 and chi_c2 modes with proportion of 50%/50%. The Jpsi forced into mu+ mu-, using BcVegPy generator. 
#                Daughter in acceptance and TightCuts are used. The efficiency is (9.1 +- 0.4)% from Generation log.
# EndDocumentation
#
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutToolWithDecay
# from Gauss.Configuration import *
# gen = Generation()
# gen.Special.addTool ( LoKi__GenCutToolWithDecay , 'TightCut' )
# gen.Special.CutTool = 'LoKi::GenCutToolWithDecay/TightCut'
# #
# tightCut = gen.Special.TightCut
# tightCut.SignalPID = 'B_c+'
# tightCut.Decay = '[B_c+ ==>  (Meson -> ^( J/psi(1S) => ^mu+ ^mu- ) ^gamma ) ^pi+]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV',
#     'inAcc        = in_range ( 0.005 , GTHETA , 0.400 ) ' ,
#     'inEcalX      = abs ( GPX / GPZ ) < 4.5 / 12.5      ' , 
#     'inEcalY      = abs ( GPY / GPZ ) < 3.5 / 12.5      ' , 
#     'inEcalHole = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 ) ' ,
#     'inEta        = in_range ( 1.9   , GETA   , 5.00  ) ' ,
#     'goodTrack    = inAcc & inEta                       ' ,   
#     'goodGamma    = ( 0 < GPZ ) & ( 150 * MeV < GPT ) & inEcalX & inEcalY & ~inEcalHole'
#     ]
# tightCut.Cuts     =    {
#     '[pi+]cc'        : 'goodTrack & ( GP  >   2.5 * GeV ) & ( GPT  >   130 * MeV ) ' , 
#     '[mu+]cc'        : 'goodTrack & ( GP  >   3.0 * GeV ) & ( GPT  >   450 * MeV ) ' ,
#     'J/psi(1S)'      : 'in_range(1.9, GY, 4.9)' ,
#     'gamma'          : 'goodGamma' ,
#     }
#
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Dmitrii Pereima
# Email: Dmitrii.Pereima@cern.ch
# Date: 20230112
# CPUTime: < 1 min
#
#
Alias      MyJ/psi      J/psi
ChargeConj MyJ/psi    MyJ/psi
Alias      Mychi_c1    chi_c1
ChargeConj Mychi_c1  Mychi_c1
Alias      Mychi_c2    chi_c2
ChargeConj Mychi_c2  Mychi_c2
#
Decay B_c+sig
  0.500    Mychi_c1   pi+   SVS ;
  0.500    Mychi_c2   pi+   STS ;
Enddecay
CDecay B_c-sig
#
Decay  Mychi_c1 
  1.000    MyJ/psi    gamma   VVP 1.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
#
Decay  Mychi_c2 
  1.000    gamma      MyJ/psi TVP ;
Enddecay
#
Decay  MyJ/psi
  1.000     mu+       mu-     VLL ;
Enddecay
#
End

