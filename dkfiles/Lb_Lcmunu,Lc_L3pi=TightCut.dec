# EventType: 15576110
#
# Descriptor: [Lambda_b0 -> (Lambda_c+ -> ( Lambda0 -> p+ pi- ) pi+ pi- pi+) mu- anti-nu_mu]cc
#
# NickName: Lb_Lcmunu,Lc_L3pi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: Lb -> Lc mu nu_mu, Lc -> Lambda pi pi pi. Loose kinematic cuts on pion from Lc decay. 
# EndDocumentation
#
# InsertPythonCode:
##
# from Configurables import LoKi__GenCutTool
# Generation().SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut  = Generation().SignalPlain.TightCut
# tightCut.Decay = "[ Lambda_b0 ==>  (Lambda_c+ ==> ^(Lambda0 ==> p+ pi- ) ^pi+ ^pi- ^pi+)  {X} {X} ^mu- nu_mu~ ]CC"
# tightCut.Preambulo += [
# "from LoKiCore.functions import in_range",
# "from GaudiKernel.SystemOfUnits import MeV, mrad",
# "inAcc = in_range(10*mrad,GTHETA,400*mrad)",
# ]
# tightCut.Cuts      =    {
#'[pi+]cc'  : "inAcc & ( GP > 2000 * MeV ) ",
#'[mu-]cc'  : "inAcc",
#'[Lambda0]cc'  : "(GCHILDCUT(inAcc, '[Lambda0 => ^p+ pi-]CC')) & (GCHILDCUT(inAcc, '[Lambda0 => p+ ^pi-]CC'))",
# }  
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Mengzhen Wang 
# Email: mengzhen.wang@cern.ch
# Date:  20231011 
# CPUTime: < 5 min
#
Alias MyLambda_c+       Lambda_c+
Alias Myanti-Lambda_c-  anti-Lambda_c-
ChargeConj MyLambda_c+  Myanti-Lambda_c-
#
Alias      MyLambda0      Lambda0
Alias      MyAntiLambda0  anti-Lambda0
ChargeConj MyLambda0      MyAntiLambda0
#
Decay Lambda_b0sig
1.0  MyLambda_c+  mu-  anti-nu_mu  PHOTOS   BaryonPCR  1 1 1 1; 
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda0
  1.0     p+   pi-      PHSP;
Enddecay
CDecay MyAntiLambda0
#
Decay MyLambda_c+
  1.0     MyLambda0   pi+  pi-  pi+      PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
End
