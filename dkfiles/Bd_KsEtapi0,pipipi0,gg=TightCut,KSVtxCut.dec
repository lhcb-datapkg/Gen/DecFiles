# EventType: 11104580
#
# Descriptor: [B0 ->  (KS0 -> pi+ pi-) (eta -> pi+ pi- (pi0 -> gamma gamma)) (pi0 -> gamma gamma)]cc
#
# NickName: Bd_KsEtapi0,pipipi0,gg=TightCut,KSVtxCut
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: < 3 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation()
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# gen.SignalRepeatedHadronization.setProp('MaxNumberOfRepetitions', 5000)
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[B0 => ^(KS0 => ^pi+ ^pi-) (eta => ^pi+ ^pi- (pi0 => ^gamma ^gamma)) ^(pi0 => gamma gamma)  ]CC'
# tightCut.Cuts      =    {
#     'gamma'          : ' InEcal' , 
#     '[pi+]cc'        : ' inAcc' , 
#     'pi0'            : ' GINTREE(goodPhoton)',
#     'KS0'            : ' decayBeforeTT'
#     }
#
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inAcc          = in_range ( 0.005 , GTHETA , 0.400 ) " ,
#     "inEcalX        =   abs ( GPX / GPZ ) < 4.5  / 12.5",
#     "inEcalY        =   abs ( GPY / GPZ ) < 3.5  / 12.5",
#     "inEcalHole     = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 )",
#     "InEcal         = inEcalX & inEcalY & ~inEcalHole ",
#     "goodPhoton     = ('gamma' == GABSID) & ( GPT > 1.8 * GeV ) & InEcal", 
#     "decayBeforeTT  = GVEV & ( GFAEVX ( GVZ , 1.e+10 ) < 2400 * millimeter)"
#     ]
#
# EndInsertPythonCode
#
# Documentation: bkgd for Bd2KsEtaGamma, gamma PT > 1.8 GeV, inAcceptance, KS0 VTZ < 2.4m
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20240527
#
Alias      MyEta   eta
ChargeConj MyEta   MyEta
Alias      Mypi0    pi0
ChargeConj Mypi0    Mypi0
#
Alias      MyK0s  K_S0
ChargeConj MyK0s  MyK0s
#
Decay B0sig
 1.0 MyK0s MyEta Mypi0 PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyEta
  1.000       pi-      pi+      Mypi0   PHSP;
Enddecay
#
Decay Mypi0
  1.000        gamma    gamma            PHSP;
Enddecay
#
Decay MyK0s
  1.0   pi+      pi-       PHSP;
Enddecay
#
End
