# EventType: 23903000
#
# Descriptor: [D_s+ => K+ K- pi+]cc
# Cuts: DaughtersInLHCbAndCutsForDFromB
# CutsOptions: DPtCuts 1600*MeV DaughtersPtMinCut 200*MeV DaughtersPtMaxCut 1500*MeV DaughtersPMinCut 1950*MeV
# FullEventCuts: ExtraParticlesInAcceptance
# CPUTime: 7 min
#
# NickName: incl_b=Ds,KKpi,3pi=DDALITZ,DecProdCut,ExtraParticles
# Documentation: Inclusive Ds3pi events from b decays. 
#                The 3pi must come from a stable b-hadron and are in LHCb acceptance.
#
# EndDocumentation
#
# InsertPythonCode:
#
# from Configurables import ExtraParticlesInAcceptance
# from GaudiKernel.SystemOfUnits import mm, mrad, MeV
#
# stable_b_hadrons = [ 511, 521, 531, 541, 5122 ]
# stable_b_hadrons += [-pid for pid in stable_b_hadrons]
#
# Generation().FullGenEventCutTool = "ExtraParticlesInAcceptance"
# Generation().addTool( ExtraParticlesInAcceptance )
# Generation().ExtraParticlesInAcceptance.WantedIDs = [211, -211]
# Generation().ExtraParticlesInAcceptance.NumWanted = 3
# Generation().ExtraParticlesInAcceptance.RequiredAncestors = stable_b_hadrons
# Generation().ExtraParticlesInAcceptance.AtLeast = True
# Generation().ExtraParticlesInAcceptance.ExcludeSignalDaughters = True
# Generation().ExtraParticlesInAcceptance.AllFromSameB = False
# Generation().ExtraParticlesInAcceptance.ZPosMax = 200.*mm
# Generation().ExtraParticlesInAcceptance.PtMin = 150.*MeV
#
# EndInsertPythonCode
#
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Benedetto G. Siddi
# Email: bsiddi@cern.ch
# Date: 20200421
#

Decay D_s+sig
  1.000        K+        K-        pi+             PHOTOS D_DALITZ;
Enddecay
CDecay D_s-sig
#
End
