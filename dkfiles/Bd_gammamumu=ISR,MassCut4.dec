# EventType: 11112207
#
# Descriptor: [B0 -> gamma mu+ mu-]cc
#
# NickName: Bd_gammamumu=ISR,MassCut4
#
#
# Cuts: DaughtersInLHCb
# 
# Documentation: 
#     ISR contribution only
#     Input parameters mu         - the scale parameter (in GeV's)
#                      Nf         - number of "effective" flavors (for b-quark Nf=5) 
#                      sr         - state radiation type
#                      res_swch   - resonant switching parametr
#                      ias        - switching parametr for \alpha_s(M_Z) value
#                      Wolfenstein parameterization for CKM matrix
#                      Egamma     - minimum energy of the photon (in GeV)
#                      mumumass_min - minimum invariant mass of the two muons (in GeV)
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Francesco Dettori
# Email: francesco.dettori@cern.ch
# Date:   20220701
# CPUTime: <1min
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay     = "[ B0 ==> mu+ mu- gamma ]CC"
# tightCut.Cuts      =    {
#     '[B0]cc'            : ' massCut ' }
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import GeV, MeV",
#     "massCut    = ( GMASS('mu+' == GID , 'mu-' == GID ) > 4000 * MeV ) " ]
#
# EndInsertPythonCode


Define mu 5.0
Define Nf 5
Define sr 0
Define res_swch 0
Define ias 1
Define Egamma 0.000001
Define A 0.8250
Define lambda 0.22509
Define barrho 0.1598
Define bareta 0.3499
Define mumumass_min 4.0
#
#
Decay B0sig
   1.000    gamma   mu+   mu-   BSTOGLLISRFSR mu Nf sr res_swch ias Egamma A lambda barrho bareta mumumass_min;
Enddecay
CDecay anti-B0sig

End
#

