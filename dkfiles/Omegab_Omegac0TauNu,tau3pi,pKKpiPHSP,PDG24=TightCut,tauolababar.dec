# EventType: 16567031
#
# Descriptor: [Xi_b- -> (Omega_c0 -> p+ K- K- pi+) (tau- -> pi- pi+ pi- nu_tau) anti-nu_tau]cc
#
# NickName: Omegab_Omegac0TauNu,tau3pi,pKKpiPHSP,PDG24=TightCut,tauolababar
#
# Cuts: LoKi::GenCutTool/TightCut
#
# ParticleValue: "Xi_b- 122 5132 -1.0 6.0458 1.64e-012 Xi_b- 5132 0.000000e+000", "Xi_b~+ 123 -5132 1.0 6.0458 1.64e-012 anti-Xi_b+ -5132 0.000000e+000", "Omega_c0   104  4332  0.0  2.6952   2.73e-013   Omega_c0   4332   0.00", "Omega_c~0   105  -4332  0.0   2.6952   2.73e-013   anti-Omega_c0  -4332   0.00"
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool(LoKi__GenCutTool ,'TightCut')
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[ Xi_b- ==> (Omega_c0 => ^p+   ^K-  ^K-  ^pi+)  (tau- => ^pi- ^pi+ ^pi- nu_tau) nu_tau~  ]CC'
# tightCut.Cuts      =    {
#     '[p+]cc'        : 'goodHad'   ,
#     '[K+]cc'        : 'goodHad'   ,
#     '[pi+]cc'       : 'goodPion' }
# tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import millimeter,micrometer,MeV,GeV',
#    'inAcc         = in_range ( 0.005 , GTHETA , 0.400 )' ,
#    'goodHad       = ( GPT > 0.1 * GeV ) & ( GP > 1. * GeV ) & inAcc ' ,
#    'goodPion      = ( GPT > 0.1 * GeV ) & ( GP > 0.5 * GeV ) & inAcc ']
# EndInsertPythonCode
#
#
# Documentation: Xi_b used a proxy for Omega_b. PDG24 numbers for Omegab and Omegac
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# CPUTime: <1min
# Responsible: Biplab Dey
# Email:  biplab.dey@cern.ch
# Date: 20241123
#
Define TauolaCurrentOption 1
Define TauolaBR1 1.
#
Alias      Mytau+         tau+
Alias      Mytau-         tau-
ChargeConj Mytau+         Mytau-
#
Alias MyOmega_c0 Omega_c0
Alias Myanti-Omega_c0 anti-Omega_c0
ChargeConj MyOmega_c0 Myanti-Omega_c0
#
Decay Xi_b-sig
  1.0    MyOmega_c0  Mytau- anti-nu_tau  PHSP;
Enddecay
CDecay anti-Xi_b+sig
#
Decay MyOmega_c0
  1.0   p+   K-  K-  pi+                  PHSP;
Enddecay
CDecay Myanti-Omega_c0
#
Decay Mytau-
 1. TAUOLA 5;
Enddecay
CDecay Mytau+
#
End
