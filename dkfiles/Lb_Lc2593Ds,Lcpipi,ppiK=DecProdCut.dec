# EventType: 15498001
# Descriptor: [Lambda_b0 -> (D_s- -> K- K+ pi-) (Lambda_c(2595)+ -> (Sigma_c0 -> (Lambda_c+ -> p+ K- pi+) pi-) pi+)]cc
# 
# NickName: Lb_Lc2593Ds,Lcpipi,ppiK=DecProdCut
# Cuts: DaughtersInLHCb
# Documentation: Decay Lambda_b0 -> (D_s- -> K- K+ pi-) ( Lambda_c(2595)+ -> (Sigma_c0 -> (Lambda_c+ -> ^p+  ^K- ^pi+ ) pi-) pi+) including intermediate states and Ds -> K K pi .
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Federica Borgato, Anna Lupato
# Email: federica.borgato@cern.ch, alupato@cern.ch
# Date: 20220907
# CPUTime: <1 min
#
Alias MySigma_c++       Sigma_c++
Alias Myanti-Sigma_c--  anti-Sigma_c--
ChargeConj MySigma_c++  Myanti-Sigma_c--
#
Alias MySigma_c0       Sigma_c0
Alias Myanti-Sigma_c0  anti-Sigma_c0
ChargeConj MySigma_c0  Myanti-Sigma_c0
#
Alias      MyLc(2593)+           Lambda_c(2593)+
Alias      MyLc(2593)-           anti-Lambda_c(2593)-
ChargeConj MyLc(2593)+           MyLc(2593)-
#
Alias      MyMainLc+             Lambda_c+
Alias      MyMainLc-             anti-Lambda_c-
ChargeConj MyMainLc+             MyMainLc-
#
Alias      MyLambda(1520)0       Lambda(1520)0
Alias      Myanti-Lambda(1520)0  anti-Lambda(1520)0
ChargeConj MyLambda(1520)0       Myanti-Lambda(1520)0
#
Alias      MyD_s+                D_s+
Alias      MyD_s-                D_s-
ChargeConj MyD_s+                MyD_s-
#
Alias      MyDelta++             Delta++
Alias      Myanti-Delta--        anti-Delta--
ChargeConj MyDelta++             Myanti-Delta--
#

Decay Lambda_b0sig
  1.0000    MyLc(2593)+       MyD_s-        PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
#BR from PDG 2020
Decay MyLc(2593)+
  0.24   MySigma_c++         pi-         PHSP;
  0.24   MySigma_c0          pi+         PHSP;
  0.18   MyMainLc+         pi+    pi-  PHSP;
Enddecay
CDecay MyLc(2593)-
#
Decay MySigma_c++
  1.0000    MyMainLc+  pi+                   PHSP;
Enddecay
CDecay Myanti-Sigma_c--
#
Decay MySigma_c0
  1.0000    MyMainLc+  pi-                     PHSP;
Enddecay
CDecay Myanti-Sigma_c0
#
Decay MyMainLc+
  0.03500 p+              K-         pi+ PHSP;
  0.01980 p+              anti-K*0       PHSP;
  0.01090 MyDelta++       K-             PHSP;
  0.02200 MyLambda(1520)0 pi+            PHSP;
Enddecay
CDecay MyMainLc-
#
Decay MyD_s-
  1 K- K+ pi- D_DALITZ;
Enddecay
CDecay MyD_s+
#
Decay MyLambda(1520)0
  1.0   p+     K-                             PHSP;
Enddecay
CDecay Myanti-Lambda(1520)0
#
Decay MyDelta++
  1 p+ pi+  PHSP;
Enddecay
CDecay Myanti-Delta--
#
End
