# EventType: 14143422
#
# Descriptor: [B_c+ -> (J/psi(1S) -> mu+ mu- ) pi+ (pi0 -> gamma gamma)]cc
#
# NickName: Bc_jpsirho,mm=BcVegPy,TightCuts,BCVHAD2
#
# Production: BcVegPy
#
# Cuts: LoKi::GenCutToolWithDecay/TightCut
#
# Documentation: The Bc decay into J/psirho+ final state with rho+ -> pi+ pi0(-> gamma gamma). The Jpsi forced into mu+ mu-, using BcVegPy generator. 
#                Daughter in acceptance and TightCuts are used. The efficiency is (4.5 +- 0.4)% from Generation log.
#		 The codes of the Bc -> VW formfactor sets used by the model are : 1 = SR, 2 = PM,
# 		 see https://gitlab.cern.ch/lhcb/Gauss/-/blob/master/Gen/EvtGen/EvtGenModels/EvtBcVHad.hh;
# 		 for description of the model and the FF sets refer to 10.1134/S1063778813050062 and https://indico.cern.ch/conferenceDisplay.py?confId=225839.
# EndDocumentation
#
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutToolWithDecay
# from Gauss.Configuration import *
# gen = Generation()
# gen.Special.addTool ( LoKi__GenCutToolWithDecay , 'TightCut' )
# gen.Special.CutTool = 'LoKi::GenCutToolWithDecay/TightCut'
# #
# tightCut = gen.Special.TightCut
# tightCut.SignalPID = 'B_c+'
# tightCut.Decay = '[B_c+ ==> ^(J/psi(1S) => ^mu+ ^mu- ) ^pi+ (pi0 -> ^gamma ^gamma)]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV',
#     'inAcc        = in_range ( 0.005 , GTHETA , 0.400 ) ' ,
#     'inEcalX      = abs ( GPX / GPZ ) < 4.5 / 12.5      ' , 
#     'inEcalY      = abs ( GPY / GPZ ) < 3.5 / 12.5      ' , 
#     'inEcalHole = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 ) ' ,
#     'inEta        = in_range ( 1.9   , GETA   , 5.00  ) ' ,
#     'goodTrack    = inAcc & inEta                       ' ,   
#     'goodGamma    = ( 0 < GPZ ) & ( 150 * MeV < GPT ) & inEcalX & inEcalY & ~inEcalHole'
#     ]
# tightCut.Cuts     =    {
#     '[pi+]cc'        : 'goodTrack & ( GP  >   2.5 * GeV ) & ( GPT  >   130 * MeV ) ' , 
#     '[mu+]cc'        : 'goodTrack & ( GP  >   3.0 * GeV ) & ( GPT  >   450 * MeV ) ' ,
#     'J/psi(1S)'      : 'in_range(1.9, GY, 4.9)' ,
#     'gamma'          : 'goodGamma' ,
#     }
#
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Dmitrii Pereima
# Email: Dmitrii.Pereima@cern.ch
# Date: 20221201
# CPUTime: < 1 min
#
Alias      MyJ/psi    J/psi
ChargeConj MyJ/psi    MyJ/psi
Alias      Mypi0      pi0
ChargeConj Mypi0      Mypi0
#
Decay B_c+sig
  1.000        MyJ/psi   pi+  Mypi0  BC_VHAD 2;
Enddecay
CDecay B_c-sig
#
Decay MyJ/psi
  1.000        mu+       mu-         VLL;
Enddecay
#
Decay Mypi0
  1.000        gamma     gamma       PHSP;
Enddecay
#
End
