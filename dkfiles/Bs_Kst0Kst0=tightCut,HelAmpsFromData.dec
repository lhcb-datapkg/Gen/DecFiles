# EventType: 13104005
#
# Descriptor: [B_s0 -> (K*(892)0 -> K+ pi-) (K*(892)~0 -> K- pi+)]cc
#
# NickName: Bs_Kst0Kst0=tightCut,HelAmpsFromData
#
# Cuts: 'LoKi::GenCutTool/TightCut'
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
#
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = '[^( B_s0 -> (K*(892)0 -> ^K+ ^pi-) (K*(892)~0 -> ^K- ^pi+) )]CC'
#
# tightCut.Preambulo += [
#   'in_acc = in_range( 0.010 , GTHETA , 0.400 )',
#   'good_track = ( GPT > 300 * MeV ) & in_acc',
#   'good_Bs = ( GPT > 3000 * MeV )'
# ]
#
# tightCut.Cuts = {
#   '[pi+]cc' : 'good_track',
#   '[K-]cc'  : 'good_track',
#   '[B_s0]cc': 'good_Bs'
# }
# EndInsertPythonCode
#
# Documentation:
#
# B_s0 decaying into two vectors K*(892)0 and K*(892)~0. Required to have a minimum pT.
# K*(892)0 and K*(892)~0 decaying into (K+ pi-) and (K- pi+), respectively.
# Kaons and pions are required to be in acceptance and have a minimum pT.
# The helicity amplitudes are chosen from measured data (https://arxiv.org/abs/1712.08683).
#
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Asier Pereiro
# Email: asier.pereiro.castro@cern.ch
# Date: 20210719
#
Define Azero   0.45607
Define pAzero  0.0
Define Aplus   0.877601
Define pAplus  2.52403
Define Aminus  0.147703
Define pAminus 0.0841355
#
Alias      MyK*0       K*0
Alias      Myanti-K*0  anti-K*0
ChargeConj MyK*0       Myanti-K*0  
#
Decay B_s0sig
  1.000    MyK*0   Myanti-K*0   SVV_HELAMP Aplus pAplus Azero pAzero Aminus pAminus;
Enddecay
CDecay anti-B_s0sig
#
Decay MyK*0
  1.000 K+ pi- VSS;
Enddecay
CDecay Myanti-K*0
#
End
