# EventType: 13102463
#
# Descriptor: [Beauty -> (phi(1020) -> K+ K-) (eta -> gamma gamma)]cc
#
# NickName: Bs_PhiEta=TightCut,Phiwidth20,HighPtEta
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
#
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[B_s0 => (phi(1020) => ^K+ ^K-) ^eta]CC'
# tightCut.Cuts      =    {
#     '[K+]cc'        : 'inAcc' , 
#     'eta'           : 'goodEta'
#     }
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inEcalX       =   abs ( GPX / GPZ ) < 4.5  / 12.5",
#     "inEcalY       =   abs ( GPY / GPZ ) < 3.5  / 12.5",
#     "inEcalHole    = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 )",
#     "InEcal        = inEcalX & inEcalY & ~inEcalHole ",
#     "inAcc         = in_range ( 0.005 , GTHETA , 0.400 ) " , 
#     "goodEta       = ('eta' == GABSID) & ( GPT > 2.5 * GeV ) & InEcal",
#     "inAcc         = in_range ( 0.005 , GTHETA , 0.400 ) ",
#     ]  
#
# EndInsertPythonCode
#
# Documentation: Bkgd for Bs->PhiG, all in PHSP, KK in acceptance, high PT eta
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20211119
#
#
Alias      Myeta        eta
ChargeConj Myeta        Myeta
#
Alias      MyPhi   phi
ChargeConj MyPhi   MyPhi
#
LSNONRELBW MyPhi
BlattWeisskopf MyPhi 0.0
Particle MyPhi 1.02 0.004
ChangeMassMin MyPhi 1.0
ChangeMassMax MyPhi 1.04
#
Decay B_s0sig
  1.000   MyPhi      Myeta         PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyPhi
  1.000   K+         K-       PHSP;
Enddecay
#
Decay Myeta
  1.000        gamma      gamma           PHSP;
Enddecay
#
End
