# EventType: 26163073
#
# Documentation:
#   Spectroscopy D0p
#   To study Lambda_c(3060) -> D0 p
#   Since Lambdac(3060) is not produced in pythia, we modify the ground state Lambdac.
# EndDocumentation
#
# Descriptor: {[ Sigma_c*+ -> (D0 -> K- pi+) p+]cc}
# NickName: Lc3060,D0p+,Kpi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# ParticleValue:  "Sigma_c*+  62  4214 1.0 3.060 9.539e-024 Sigma_c*+ 4214 1.0e-004", "Sigma_c*~- 63 -4214 -1.0 3.060 9.539e-024 anti-Sigma_c*-  -4214 1.0e-004"
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# #
# tightCut.Decay    = "^[ Sigma_c*+ -> (D0 -> ^K- ^pi+) ^p+ ]CC"
# tightCut.Cuts     = {
#    '[Sigma_c*+]cc' : "in_range ( 0.00 , num/den , 1.00 )",
#    '[K-]cc'        : "in_range ( 0.010 , GTHETA , 0.400 )",
#    '[pi+]cc'       : "in_range ( 0.010 , GTHETA , 0.400 )",
#    '[p+]cc'        : "in_range ( 0.010 , GTHETA , 0.400 )",
#    }
# #
# tightCut.Preambulo += [
#    "from LoKiGen.decorators import *",
#    "from LoKiCore.functions import *",
#    "from LoKiCore.math import sqrt",
#    "D_PX   = GCHILD(GPX,'p+' == GABSID)",
#    "D_PY   = GCHILD(GPY,'p+' == GABSID)",
#    "D_PZ   = GCHILD(GPZ,'p+' == GABSID)",
#    "D_E    = GCHILD(GE,'p+' == GABSID)",
#    "Q_PX   = GPX",
#    "Q_PY   = GPY",
#    "Q_PZ   = GPZ",
#    "Q_E    = GE",
#    "D_M    = sqrt(D_E*D_E - D_PX*D_PX - D_PY*D_PY - D_PZ*D_PZ)",
#    "Q_M    = sqrt(Q_E*Q_E - Q_PX*Q_PX - Q_PY*Q_PY - Q_PZ*Q_PZ)",
#    "PdotD  = 2.0*Q_E*D_E",
#    "PdotQ  = 2.0*Q_E*Q_E",
#    "DdotQ  = Q_E*D_E - D_PX*Q_PX - D_PY*Q_PY - D_PZ*Q_PZ",
#    "Q2     = Q_M*Q_M",
#    "P2     = 4.0*Q_E*Q_E",
#    "D2     = D_M*D_M",
#    "num    = PdotD*Q2 - PdotQ*DdotQ",
#    "den    = sqrt((PdotQ*PdotQ - Q2*P2)*(DdotQ*DdotQ - Q2*D2))",
#    ]
# EndInsertPythonCode
#
# CPUTime: <1min
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Ignacio A Monroy
# Email: imonroyc@cern.ch
# Date: 20200401
#
Alias MyD0      D0
Alias MyantiD0  anti-D0
ChargeConj MyD0 MyantiD0
#
Decay Sigma_c*+sig
1.000   MyD0 p+          PHSP;
Enddecay
CDecay anti-Sigma_c*-sig
#
Decay MyD0
1.000   K-  pi+   PHSP;
Enddecay
CDecay MyantiD0
#
End
