# EventType: 12143402
#
# Descriptor: [B+ ->  (J/psi(1S) -> mu+ mu-) (K*(892)+ -> K+ pi0) ]cc
#
# NickName: Bu_JpsiKst,mm,Kpi0=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# # 
# from Configurables import LoKi__GenCutTool 
# gen = Generation() 
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# # 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[ B+  =>  ^( J/psi(1S) => ^mu+ ^mu-) (K*(892)+ => ^K+ (pi0 -> ^gamma ^gamma))]CC'
# tightCut.Cuts      =    {
#     'gamma'     : ' goodGamma ' ,
#     '[mu+]cc'   : ' goodMuon  ' , 
#     '[K+]cc'    : ' goodKaon  ' , 
#     'J/psi(1S)' : ' goodPsi   ' }
# tightCut.Preambulo += [
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) ' , 
#     'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5      ' , 
#     'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5      ' , 
#     'goodMuon  = ( GPT > 500  * MeV ) & ( GP > 6 * GeV )     & inAcc   ' , 
#     'goodKaon  = ( GPT > 150  * MeV )                        & inAcc   ' , 
#     'goodGamma = ( 0 < GPZ ) & ( 150 * MeV < GPT ) & inEcalX & inEcalY ' ,
#     'goodPsi   = ( GPT > 500  * MeV ) & in_range ( 1.8 , GY , 4.5 )    ' ]
#
# EndInsertPythonCode
#
# Documentation:
#   Tight generator level cuts applied for all final state particles,
#   which increases the statistics with the factor of ~3.
# EndDocumentation
#
# PhysicsWG: B2Ch
# Tested: Yes
# Responsible: Max Chefdeville
# Email: chefdevi@lapp.in2p3.fr
# Date: 20200428
#
## CPUTime:     < 1 min
#
Alias      MyJ/psi    J/psi
Alias      MyK*+      K*+
Alias      MyK*-      K*-
ChargeConj MyK*+      MyK*-
ChargeConj MyJ/psi    MyJ/psi
#
Decay B+sig
  1.000         MyJ/psi   MyK*+    SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus;
Enddecay
Decay B-sig
  1.000         MyJ/psi   MyK*-		 SVV_HELAMP PKHplus PKphHplus PKHzero PKphHzero PKHminus PKphHminus;
Enddecay
#
Decay MyJ/psi
  1.000         mu+       mu-            PHOTOS VLL;
Enddecay
#
Decay MyK*+
  1.000         K+        pi0            PHSP;
Enddecay
CDecay MyK*-
#
End

