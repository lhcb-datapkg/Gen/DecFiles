# EventType: 11102406
#
# Descriptor: {[[B0]nos -> pi+ pi- (pi0 -> gamma gamma)]cc, [[B0]os -> pi- pi+ (pi0 -> gamma gamma)]cc}
#
# NickName: Bd_pi+pi-pi0=TighterCuts,sqDalitz
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = '[^(B0 => ^pi+ ^pi- ^(pi0 -> ^gamma ^gamma))]CC'
# tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV',
#    'inAcc     = in_range (0.005, GTHETA, 0.400) & in_range ( 2.0 , GETA , 5.0)',
#    'inEcalX   = abs ( GPX / GPZ ) < 4.5 / 12.5 ' ,
#    'inEcalY   = abs ( GPY / GPZ ) < 3.5 / 12.5 ' ,
#    'goodB     = (GP > 25000 * MeV) & (GPT > 3500 * MeV)',
#    'goodP     = in_range ( 4.0 * GeV , GP , 300 * GeV) & (GPT >  495 * MeV) & inAcc',
#    'goodK     = in_range ( 4.0 * GeV , GP , 400 * GeV) & (GPT >  495 * MeV) & inAcc',
#    'goodPi0   = (GPT >  1000 * MeV)',
#    'goodGamma = ( GPZ > 400 * MeV ) & inEcalX & inEcalY',
#    'B_FD      = (GTIME > 70 * micrometer)',
# ]
# tightCut.Cuts = {
#    '[B0]cc'   : 'goodB & B_FD',
#    '[pi-]cc'  : 'goodP',
#    '[pi+]cc'   : 'goodK',
#    '[pi0]cc'  : 'goodPi0',
#    'gamma'    : 'goodGamma'
# }
#
# EndInsertPythonCode
#
# Documentation: flat in square Dalitz, pi0 forced to gamma gamma, acceptance and pT cuts on decay products.
# EndDocumentation
#
# PhysicsWG: BnoC
# Tested: Yes 
# CPUTime: 2 mins 
# Responsible: Yiduo Shang
# Email: yshang@cern.ch
# Date: 20230515
#
Alias      Mypi0   pi0
ChargeConj Mypi0   Mypi0
Decay B0sig
1.000       pi+      pi-     Mypi0     FLATSQDALITZ;
Enddecay
CDecay anti-B0sig
#
Decay Mypi0
  1.000     gamma   gamma             PHSP;
Enddecay
#
End
#
