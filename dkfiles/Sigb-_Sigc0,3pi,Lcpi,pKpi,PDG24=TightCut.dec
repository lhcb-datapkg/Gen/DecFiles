# EventType: 16267001 
#
# Descriptor: [Sigma_b- -> (Sigma_c0 -> (Lambda_c+ -> p+ K- pi+) pi-) pi- pi- pi+]cc
#
# NickName: Sigb-_Sigc0,3pi,Lcpi,pKpi,PDG24=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# ParticleValue: "Sigma_b-   114   5112 -1.0  5.81564  2.506e-21  Sigma_b-   5112  0.000000e+00", "Sigma_b~+  115  -5112  1.0  5.81564  2.506e-21  anti-Sigma_b+  -5112  0.000000e+00"
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool(LoKi__GenCutTool ,'TightCut')
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[Sigma_b- ==> (Sigma_c0 => (Lambda_c+ ==> ^p+ ^K- ^pi+) ^pi-)  ^pi- ^pi- ^pi+ ]CC'
# tightCut.Cuts      =    {
#     '[p+]cc'        : 'goodHad'   ,
#     '[K+]cc'        : 'goodHad'   ,
#     '[pi+]cc'       : 'goodPion' }
# tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import millimeter,micrometer,MeV,GeV',
#    'inAcc         = in_range ( 0.005 , GTHETA , 0.400 )' ,
#    'goodHad       = ( GPT > 0.1 * GeV ) & ( GP > 1. * GeV ) & inAcc ' ,
#    'goodPion      = ( GPT > 0.1 * GeV ) & ( GP > 0.5 * GeV ) & inAcc ' ]
# EndInsertPythonCode
#
#
# Documentation: PDG24 numbers for Sigmab+. Sigb+ -> (Sigc++ -> (Lc -> pKpi) pi+) 3pi
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: 2 min
# Responsible: Biplab Dey
# Email:  biplab.dey@cern.ch
# Date: 20241123
#
Alias MySigma_c0       Sigma_c0
Alias Myanti-Sigma_c0  anti-Sigma_c0
ChargeConj MySigma_c0  Myanti-Sigma_c0
#
# Define Lambda_c+
Alias      MyLambda_c+      Lambda_c+
Alias      Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+      Myanti-Lambda_c-
#
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
Alias      MyDelta++       Delta++
Alias      Myanti-Delta--  anti-Delta--
ChargeConj MyDelta++       Myanti-Delta--
#
Decay MySigma_c0
  1.0000 MyLambda_c+     pi-              PHSP;
Enddecay
CDecay Myanti-Sigma_c0
#
Decay Sigma_b-sig
  1.0    MySigma_c0  pi- pi+ pi-          PHSP;
Enddecay
CDecay anti-Sigma_b+sig
#
Decay MyLambda_c+
  0.193 MyDelta++ K-                                   PHSP;
  0.239 p+        Myanti-K*0                           PHSP;
  0.568 p+        K-      pi+                          PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyK*0
  1.000   K+  pi-                             VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyDelta++
  1.0000  p+  pi+                             PHSP;
Enddecay
CDecay Myanti-Delta--
#
End
