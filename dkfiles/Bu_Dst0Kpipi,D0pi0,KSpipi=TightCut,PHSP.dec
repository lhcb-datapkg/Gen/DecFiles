# EventType: 12267500
#
# Descriptor: [B+ -> (D*(2007)~0 -> (D~0 -> (KS0 -> pi+ pi-) pi+ pi-) (pi0 -> gamma gamma)) K+ pi- pi+]cc
#
# NickName: Bu_Dst0Kpipi,D0pi0,KSpipi=TightCut,PHSP
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# Generation().SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '^[B+ ==> ^(D*(2007)~0 ==> ^(D~0 => ^(KS0 ==> ^pi+ ^pi-) ^pi+ ^pi-) ^(pi0 ==> ^gamma ^gamma)) ^K+ ^pi- ^pi+]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'inAcc        = (in_range (0.005, GTHETA, 0.400))',
#     'goodB        = (GP > 55000 * MeV) & (GPT > 5000 * MeV) & (GTIME > 0.135 * millimeter)',
#     'goodD        = (GP > 25000 * MeV) & (GPT > 2500 * MeV)',
#     'goodKS       = (GFAEVX(abs(GVZ), 0) < 2500.0 * millimeter)',
#     'goodDDaugPi  = (GNINTREE (("pi+" == GABSID) & (GP > 2000 * MeV) & inAcc, 4) > 3.5)',
#     'goodKsDaugPi = (GNINTREE (("pi+" == GABSID) & (GP > 2000 * MeV) & inAcc, 4) > 1.5)',
#     'goodBachKPia  = (GNINTREE ((("K+" == GABSID) | ("pi+" == GABSID)) & (GP > 2000 * MeV) & (GPT > 100 * MeV) & inAcc, 4) > 4.5)',
#     'goodBachKPib  = (GNINTREE ((("K+" == GABSID) | ("pi+" == GABSID)) & (GP > 2000 * MeV) & (GPT > 300 * MeV) & inAcc, 4) > 1.5)',
# ]
# tightCut.Cuts      =    {
#     '[B+]cc'         : 'goodB  & goodBachKPia & goodBachKPib',
#     '[D0]cc'         : 'goodD  & goodDDaugPi',
#     '[KS0]cc'        : 'goodKS & goodKsDaugPi',
#     '[pi+]cc'        : 'inAcc',
#     '[K+]cc'         : 'inAcc',
#     '[pi0]cc'        : 'inAcc',
#     '[gamma]cc'      : 'inAcc',
#     '[D*(2007)0]cc'  : 'inAcc'
#     }
# EndInsertPythonCode
#
# Documentation: B decays to D*0 K pi pi, D*0 -> D0 pi0, D0 -> KS pi pi. Decay products in acceptance and tight cuts
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: <1min
# Responsible: George Lovell
# Email: george.lovell@cern.ch
# Date: 20190405
#
Alias MyD*0 D*0
Alias Myanti-D*0 anti-D*0
ChargeConj MyD*0 Myanti-D*0
#
Alias      MyD0        D0
Alias      Myanti-D0   anti-D0
ChargeConj MyD0        Myanti-D0
#
Alias      MyK_S0      K_S0
ChargeConj MyK_S0      MyK_S0
#
Alias      MyK*0       K*0
Alias      Myanti-K*0  anti-K*0
ChargeConj MyK*0       Myanti-K*0
#
Alias      MyK_1-      K_1-
Alias      MyK_1+      K_1+
ChargeConj MyK_1+      MyK_1-
#
Alias      MyK_2*-     K_2*-
Alias      MyK_2*+     K_2*+
ChargeConj MyK_2*+     MyK_2*-
#
Alias      MyK'_1-     K'_1-
Alias      MyK'_1+     K'_1+
ChargeConj MyK'_1+     MyK'_1-
#
Alias      MyK''*-     K''*-
Alias      MyK''*+     K''*+
ChargeConj MyK''*+     MyK''*-
#
Alias      MyK'*-      K'*-
Alias      MyK'*+      K'*+
ChargeConj MyK'*+      MyK'*-
#
Alias      MyK_0*0      K_0*0
Alias      Myanti-K_0*0 anti-K_0*0
ChargeConj MyK_0*0      Myanti-K_0*0 
#
Alias      Myomega omega
ChargeConj Myomega Myomega
#
Alias      Mypi0     pi0
ChargeConj Mypi0     Mypi0
#
Decay B+sig
0.34   Myanti-D*0 MyK_1+                     SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0; 
0.20   Myanti-D*0 MyK'_1+                    SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0; 
0.07   Myanti-D*0  MyK*0  pi+                PHSP; 
0.07   MyK_2*+ Myanti-D*0                    PHSP;
0.20   Myanti-D*0 MyK'*+                     SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;  
0.12   Myanti-D*0 MyK''*+                    SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0; 
Enddecay
CDecay B-sig
#
Decay Myanti-D0
  1.000        MyK_S0    pi+    pi- PHSP;
Enddecay
CDecay MyD0
#
Decay MyD*0
  1.000    MyD0  Mypi0                        VSS;
Enddecay
CDecay Myanti-D*0
#
Decay MyK*0
  1.000 K+   pi-                   VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyK_0*0
  1.000 K+ pi-                    PHSP;
Enddecay
CDecay Myanti-K_0*0
#
Decay MyK_2*+
  1.000 MyK*0 pi+                    PHSP;
Enddecay
CDecay MyK_2*-
#
Decay MyK'*+                            
  0.90   MyK*0 pi+                   VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.10   rho0 K+                     VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyK'*-

Decay MyK''*+                            
  0.67   MyK*0 pi+                   VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.33   rho0 K+                     VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyK''*-
#
Decay Myomega
  1.000 pi-  pi+                       VSS;
Enddecay
#
Decay MyK_1+
  0.140  rho0   K+                    VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
  0.071  MyK*0  pi+                   VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
  0.116  MyK_0*0  pi+                 PHSP;
  0.0019 Myomega K+                   VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyK_1-
#
Decay MyK'_1+
  0.6300   MyK*0  pi+                        VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0100   rho0 K+                         VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.0133   K+  pi+  pi-                    PHSP;
Enddecay
CDecay MyK'_1-
#
Decay MyK_S0
  1.000    pi+ pi-        PHSP;
Enddecay
#
Decay Mypi0 
  1.000      gamma   gamma   PHSP;
Enddecay  
#

End

