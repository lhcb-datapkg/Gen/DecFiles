DecFiles v30r99 2023-12-13 
==========================  
 
! 2023-12-13 - Michal Kreps (MR !1725)
   Update Gauss version to v49r25 in documentation

! 2023-12-13 - Kai Kang (MR !1701)  
   Add 3 new decay files  
   + 26163410 : Sigmac_Lcpi0,Lc_pKpi=DecProdCut  
   + 26163411 : Sigmacstar_Lcpi0,Lc_pKpi=DecProdCut  
   + 26163412 : Xic_Lcpi0,Lc_pKpi=DecProdCut  
  
! 2023-12-11 - Salil Joshi (MR !1700)  
   Add 2 new decay files  
   + 11146401 : Bd_Psi2Spi0Kpi,Jpsipipi,mm=DecProdCut,PHSP  
   + 11144470 : Bd_Psi2Spi0Kpi,mm=DecProdCut,PHSP  
  
! 2023-12-11 - Ellinor Eckstein (MR !1698)  
   Add 4 new decay files  
   + 15168100 : Lb_LLpmDsp,L_ppi,Dsp_KKpi=DecProdCut  
   + 15106103 : Lb_LLpmK,L_ppi=DecProdCut  
   + 16168140 : Xib_LLpmDsp,L_ppi,Dsp_KKpi=DecProdCut  
   + 16106140 : Xib_LLpmKp,L_ppi=DecProdCut  
  
! 2023-12-05 - Fernanda Goncalves Abrantes (MR !1696)  
   Add 7 new decay files  
   + 13166116 : Bs_LambdacbarLambdabarpi+S,pbarKpi=DecProdCut,ppi=DecProdCut,mS=1500MeV  
   + 13166115 : Bs_LambdacbarLambdapi+,pbarKpi=DecProdCut,ppi=DecProdCut  
   + 13166060 : Bs_LambdacbarpK-pi+,pbarKpi=DecProdCut  
   + 13166061 : Bs_LambdacpbarK+pi+S,pbarKpi=DecProdCut,mS=1500MeV  
   + 13104190 : Bs_Lambdapbarpi+,ppi=DecProdCut  
   + 13104191 : Bs_pbarLambdabarpi+S,ppi=DecProdCut,mS=1875MeV  
   + 13104088 : Bs_pbarpbarK+pi+S=DecProdCut,mS=1875MeV  
  
! 2023-12-01 - Youhua Yang (MR !1693)  
   Add 10 new decay files  
   + 12135041 : Bu_JpsiphiK,pp=DecProdCut  
   + 12135401 : Bu_JpsiphiK,pppi0KK=DecProdCut  
   + 12135053 : Bu_etacphiK,KK=DecProdCut  
   + 12137004 : Bu_etacphiK,KKKK=DecProdCut  
   + 12137001 : Bu_etacphiK,KKpipi=DecProdCut  
   + 12137101 : Bu_etacphiK,KsKpi=DecProdCut  
   + 12135052 : Bu_etacphiK,pipi=DecProdCut  
   + 12137002 : Bu_etacphiK,pipipipi=DecProdCut  
   + 12137003 : Bu_etacphiK,pipipp=DecProdCut  
   + 12135050 : Bu_etacphiK,pp=DecProdCut  
  
! 2023-12-01 - Jose Ivan Cambon Bouzas (MR !1692)  
   Add 5 new decay files  
   + 27163492 : Ds2317_Dspi0,KKpi,Dalitz=TightCut  
   + 27163284 : Ds2460_Dsgamma,KKpi,Dalitz=TightCut  
   + 27163686 : Ds2460_Dsstpi0,Dsgamma,KKpi,Dalitz=TightCut  
   + 27163223 : Dsst_Dsgamma,KKpi,Dalitz=TightCut  
   + 27163422 : Dsst_Dspi0,KKpi,Dalitz=TightCut  
  
! 2023-11-27 - Vsevolod Yeroshenko (MR !1691)  
   Add 3 new decay files  
   + 10134200 : incl_b=Jpsi,LstLstgamma,pK,InAcc  
   + 10134010 : incl_b=Jpsi,phippbar,KK,InAcc  
   + 10136002 : incl_b=chic0,LstLstpipi,pK,InAcc  
  
! 2023-11-22 - Kunpeng Yu (MR !1689)  
   Add 2 new decay files  
   + 15298016 : Lb_LcDs2460,pKpi,Dspipi,KKpi=DecProdCut  
   + 15298015 : Lb_LcDspipi,pKpi,KKpi=TightCut,LowLcpipiMass2680MeV,EvtGenCut  
  
! 2023-11-21 - Kai Kang (MR !1688)  
   Add new decay file  
   + 15146001 : Lb_psi2SpK,pipimumu=phsp,DecProdCut  
  
! 2023-11-17 - Jessy Daniel (MR !1687)  
   Add 4 new decay files  
   + 12265501 : Bu_D0K,K1Pi,Kspipipi0=TightCut  
   + 12165594 : Bu_D0K,Kst0PiPi,Kspipipi0=TightCut,PHSP  
   + 12265500 : Bu_D0pi,K1Pi,Kspipipi0=TightCut  
   + 12165593 : Bu_D0pi,Kst0PiPi,Kspipipi0=TightCut,PHSP  
  
! 2023-11-17 - Keira Gwyn Farmer (MR !1686, !1699)  
   Add new decay file  
   + 42972001 : ZInccharm=mumu,charged,InAcc  
  
! 2023-11-08 - Jan Langer (MR !1685)  
   Add 8 new decay files  
   + 11196094 : Bd_Dst-Dst+,D0pi-,KK=DecProdCut,SVVNONCPEIGEN  
   + 11196093 : Bd_Dst-Dst+,D0pi-,Kpi=DecProdCut,SVVNONCPEIGEN  
   + 11196096 : Bd_Dst-Dst+,D0pi-,piK=DecProdCut,SVVNONCPEIGEN  
   + 11196095 : Bd_Dst-Dst+,D0pi-,pipi=DecProdCut,SVVNONCPEIGEN  
   + 13196057 : Bs_Dst-Dst+,D0pi-,KK=DecProdCut,HELAMP  
   + 13196056 : Bs_Dst-Dst+,D0pi-,Kpi=DecProdCut,HELAMP  
   + 13196059 : Bs_Dst-Dst+,D0pi-,piK=DecProdCut,HELAMP  
   + 13196058 : Bs_Dst-Dst+,D0pi-,pipi=DecProdCut,HELAMP  
  
! 2023-11-07 - Emir Muhammad (MR !1684)  
   - Overhaul the Decparser CI test

! 2023-11-07 - Miroslav Saur (MR !1683)  
   Modify decay file  
   + 16107134 : Xibm_XimKpKpKmKm,ppi=TightCut,AngularCut  
  
! 2023-11-06 - Zan Ren (MR !1682)  
   Add new decay file  
   + 15146304 : Lb_JpsiLambdaetap,mm,grho,pipi=phsp,DecProdCut  
  
! 2023-11-03 - Ned Francis Howarth (MR !1681)  
   Add 2 new decay files  
   + 15896010 : Lb_LcLcn,pKpi,pKpi=PHSP,DecProdCut,cocktail,knownBKGs  
   + 15896011 : Lb_LcLcn,pKpi,pKpi=PHSP,DecProdCut,cocktail,unknownBKGs  
  
! 2023-11-02 - Gaelle Khreich (MR !1680)  
   Add new decay file  
   + 13124030 : Bs_phiee,flatq2=DecProdCut,TightCut600MeV  
  
! 2023-10-30 - Salil Joshi (MR !1679)  
   Add 3 new decay files  
   + 11146400 : Bd_Jpsipipipi0Kpi,mm=DecProdCut,PHSP  
   + 11246400 : Bd_X3940Kpi,JpsiOmega,mm=DecProdCut,PHSP  
   + 12147000 : Bu_Psi4660K,JpsiPiPiPiPi,mm=DecProdCut,PHSP  
  
! 2023-10-26 - Thomas Oeser (MR !1676)  
   Add new decay file  
   + 12115180 : Bu_Kstmumu,KSpi=PHSP,flatq2,DecProdCut  
  
! 2023-10-21 - Youhua Yang (MR !1660)  
   Add new decay file  
   + 26105990 : Xic_OmegaKpi,LambdaK=phsp,TightCut  
  
