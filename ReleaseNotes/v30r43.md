!========================= DecFiles v30r43 2020-02-26 =======================  
  
! 2020-02-25 - Matthew William Kenzie (MR !422)  
   Add 4 new decay files  
   + 11166110 : Bd_D0Kpi,KSKK=BsqDalitz,DDalitz,DecProdCut  
   + 11166109 : Bd_D0Kpi,KSKK=BsqDalitz,DPHSP,DecProdCut  
   + 11166108 : Bd_D0Kpi,KSpipi=BsqDalitz,DDalitz,DecProdCut  
   + 11166107 : Bd_D0Kpi,KSpipi=BsqDalitz,DPHSP,DecProdCut  
  
! 2020-02-25 - Alessandra Pastore (MR !421)  
   Add 2 new decay files  
   + 11166171 : Bd_Dst-KSpi,D0pi,Kpi=DecProdCut  
   + 11166002 : Bd_Dstpipipi,D0pi=DecProdCut  
  
! 2020-02-22 - Ao Xu (MR !420)  
   Add new decay file  
   + 26166063 : Xicc++_Xic0pipi,pKKpi-res=GenXicc,DecProdCut,WithMinPT  
  
! 2020-02-21 - Marco Pappagallo (MR !419)  
   Add new decay file  
   + 16165932 : Xib_LcKpi,pKpi=TightCut,mLcpi3000MeV  
  
! 2020-02-19 - Fidan Suljik (MR !417)  
   Add new decay file  
   + 12167171 : Bu_D0Kst+,KSKK,KSpi=DecProdCut  
  
! 2020-02-18 - Elisabeth Maria Niel (MR !415)  
   Add 4 new decay files  
   + 24142005 : exclu_Jpsi,mm=coherent_starlight  
   + 24142006 : exclu_Jpsi,mm=incoherent_starlight  
   + 28142006 : exclu_psi2S,mm=coherent_starlight  
   + 28142007 : exclu_psi2S,mm=incoherent_starlight  
  
! 2020-02-14 - Giovanni Cavallero (MR !413)  
   Add 2 new decay files  
   + 28144252 : X3872_chic1pipi,Jpsigamma=TightCut  
   + 28144253 : Y4260_X3872gamma,Jpsirho=TightCut  
  
! 2020-02-04 - Giulia Frau (MR !412)  
   Add new decay file  
   + 23513203 : Ds_etamunu,gmm=Eta2MuMuGamma,TightCut  
  
! 2018-04-24 - Philip James Ilten (MR !71)  
   Add 6 new decay files  
   + 49142110 : cep_chic0_psi1Sgamma,mumu  
   + 49142111 : cep_chic1_psi1Sgamma,mumu  
   + 49142112 : cep_chic2_psi1Sgamma,mumu  
   + 49942001 : cep_psi2S_psi1SX,mumu  
   + 49144001 : cep_psi2S_psi1Spipi,mumu  
  
