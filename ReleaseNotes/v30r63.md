DecFiles v30r63 2021-09-01 
==========================  
 
! 2021-08-19 - Wei Yilei (MR !820)  
   Add new decay file  
   + 12125040 : Bu_phiKee,KK=DecProdCut  
  
! 2021-08-18 - Biplab Dey (MR !819)  
   Add new decay file  
   + 15246105 : Lb_JpsiKsppi,mm=TightCut,KSVtxCut,pKst892,Pc4100cocktail  
  
! 2021-08-08 - Qiaohong Li (MR !815)  
   Add new decay file  
   + 11244502 : Bd_JpsiKspipi,mm,pipi=DecProdCut,pCut1600MeV  
  
! 2021-08-06 - Michal Kreps (MR !813)  
   Fix typo in decparser. Add phi to list of signal particles for general flag 3.
     
! 2021-07-28 - Xijun Wang (MR !805)  
   Add new decay file  
   + 15146164 : Lb_psi2SLambda,Jpsipipi,mm=VVPIPI,DecProdCut,pCut1600MeV  
   Modify decay file  
   + 15146162 : Lb_X38721++Lambda,Jpsirho,pipi,mm=DecProdCut,pCut1600MeV  
  
