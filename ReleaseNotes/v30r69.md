DecFiles v30r69 2021-12-16 
==========================  
 
! 2021-12-16 - Michal Kreps (MR !943)  
   Change version of Gauss for tests to v49r22  
  
! 2021-12-14 - Miguel Fernandez Gomez (MR !941)  
   Modify 3 decay files  
   + 21115050 : D+_pi+eta,pipimumu,etaModel=DecProdCut  
   + 21115020 : D+_pi+etap,pipimumu,etapModel=DecProdCut  
   + 23115030 : Ds_pi+eta,pipimumu,etaModel=DecProdCut  
  
! 2021-12-13 - Sergio Jaimes (MR !940)  
   Add new decay file  
   + 16576140 : Xib0_Xicmunu,Xipipi,Lambda0pi,ppi=DecProdCut  
  
! 2021-12-13 - Miguel Fernandez Gomez (MR !939)  
   Add 3 new decay files  
   + 21115050 : D+_pi+eta,pipimumu,etaModel=DecProdCut  
   + 21115020 : D+_pi+etap,pipimumu,etapModel=DecProdCut  
   + 23115030 : Ds_pi+eta,pipimumu,etaModel=DecProdCut  
  
! 2021-12-13 - Pablo Baladron Rodriguez (MR !938)  
   Add 2 new decay files  
   + 12105163 : Bu_KsKmpipi,PHSP=TightCut  
   + 12105162 : Bu_KsKppipi,PHSP=TightCut  
  
! 2021-12-13 - Menglin Xu (MR !937)  
   Modify 2 decay files  
   + 49011014 : ccbar=HardQCD,pt14GeV,mu  
   + 49011004 : ccbar=HardQCD,pt18GeV,mu  
  
! 2021-12-10 - Albert Bursche (MR !936)  
   Add new decay file  
   + 30122003 : exclu_ee,gg=coherent_starlight_with_loose_cuts  
  
! 2021-12-10 - Giulia Tuci (MR !935)  
   Add 10 new decay files  
   + 21103221 : D+_etaprimeK,rhogamma=DecProdCut  
   + 21103211 : D+_etaprimepi,rhogamma=DecProdCut  
   + 25103600 : Lc_etaSigma,pipigppi0,gg=DecProdCut  
   + 25103410 : Lc_etaSigma,pipipi0ppi0,gg=DecProdCut  
   + 25103200 : Lc_etap,pipig=DecProdCut  
   + 25103430 : Lc_etap,pipipi0,gg=DecProdCut  
   + 25103420 : Lc_etaprimeSigma,pipietappi0,gggg=DecProdCut  
   + 25103610 : Lc_etaprimeSigma,rho0gppi0,pipigg=DecProdCut  
   + 25103440 : Lc_etaprimep,pipieta,gg=DecProdCut  
   + 25103201 : Lc_etaprimep,rho0g,pipi=DecProdCut  
  
! 2021-12-09 - Anastasiia Kozachuk (MR !934)  
   Add new decay file  
   + 15266096 : Lb_Lc3pi,pKpi-res=TightCut  
  
! 2021-11-24 - Dmitrii Pereima (MR !924)  
   Add 5 new decay files  
   + 14147011 : Bc_jpsikkpipipi,mm=BcVegPy,DecProdCut,BCVHAD  
   + 14147012 : Bc_jpsikpipipipi,mm=BcVegPy,DecProdCut,BCVHAD  
   + 14147010 : Bc_jpsipipipipipi,mm=BcVegPy,DecProdCut,BCVHAD  
   + 14149003 : Bc_jpsipipipipipipipi,mm=BcVegPy,DecProdCut,BCVHAD  
   + 14147013 : Bc_psi2Spipipi,jpsipipi,mm=BcVegPy,DecProdCut,BCVHAD  
  
! 2021-11-22 - Albert Bursche (MR !923)  
   Add 16 new decay files  
   + 24142012 : exclu_Jpsi,mm=coherent_starlight_evtGen_longitudinal_large_window  
   + 24142010 : exclu_Jpsi,mm=coherent_starlight_evtGen_transverse_large_window  
   + 24142013 : exclu_Jpsi,mm=incoherent_starlight_evtGen_longitudinal_large_window  
   + 24142011 : exclu_Jpsi,mm=incoherent_starlight_evtGen_transverse_large_window  
   + 28142028 : exclu_psi,jpsipippim=coherent_starlight_evtGen_longitudinal_large_window  
   + 28142024 : exclu_psi,jpsipippim=coherent_starlight_evtGen_transverse_large_window  
   + 28142029 : exclu_psi,jpsipippim=incoherent_starlight_evtGen_longitudinal_large_window  
   + 28142025 : exclu_psi,jpsipippim=incoherent_starlight_evtGen_transverse_large_window  
   + 28142030 : exclu_psi,jpsipizpiz=coherent_starlight_evtGen_longitudinal_large_window  
   + 28142026 : exclu_psi,jpsipizpiz=coherent_starlight_evtGen_transverse_large_window  
   + 28142031 : exclu_psi,jpsipizpiz=incoherent_starlight_evtGen_longitudinal_large_window  
   + 28142027 : exclu_psi,jpsipizpiz=incoherent_starlight_evtGen_transverse_large_window  
   + 28142022 : exclu_psi,mm=coherent_starlight_evtGen_longitudinal_large_window  
   + 28142020 : exclu_psi,mm=coherent_starlight_evtGen_transverse_large_window  
   + 28142023 : exclu_psi,mm=incoherent_starlight_evtGen_longitudinal_large_window  
   + 28142021 : exclu_psi,mm=incoherent_starlight_evtGen_transverse_large_window  
  
! 2021-11-22 - Simone Stracka (MR !922)  
   Add 2 new decay files  
   + 25103270 : Lc_etap,pipigamma=TightCut  
   + 25103221 : Lc_etaprimep,rhogamma=TightCut  
  
! 2021-10-12 - Hanae Tilquin (MR !850)  
   Add 4 new decay files  
   + 11676042 : Bd_Dphimunu,Kpimunu=KKmumuInAcc  
   + 12575043 : Bu_D0phimunu,Kmunu=KKmumuInAcc  
   + 12595042 : Bu_DsD0,phimunu,Kmunu=KKmumuInAcc  
   + 12575044 : Bu_DsKmunu,phimunu=KKmumuInAcc  
  
! 2021-10-12 - Hanae Tilquin (MR !849)  
   Add 5 new decay files  
   + 11696442 : Bd_DDK,Kmunu,KmunuCocktail=KKmumuInAcc  
   + 11596242 : Bd_DDs,Kpimunu,phimunuCocktail=KKmumuInAcc  
   + 13596242 : Bs_DsDs,phimunu,phimunu=KKmumuInAcc  
   + 15595142 : Lb_LambdacDs,n0KS0munu,KKmunu=KKmumuInAcc  
   + 15696042 : Lb_LambdacDs,pKmunu,phimunu=KKmumuInAcc  
  
! 2021-08-13 - Hanae Tilquin (MR !818)  
   Add 2 new decay files  
   + 13694242 : Bs_DsDs,KKmunu,munu=TightCut  
   + 13874252 : Bs_Dstaunu,phimunu,mununu=TightCut  
  
