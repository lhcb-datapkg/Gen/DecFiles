DecFiles v30r83 2023-01-19 
==========================  
 
! 2023-01-19 - Michal Kreps (MR !1270)  
   Modify decay file (new event type) 
   + 15576102 : Lb_Lcpipimunu,Lambdapi=TightCut,LambdaTT  
  
! 2023-01-18 - David Bacher (MR !1269)  
   Add new decay file  
   + 31111201 : tau_mugamma=DecProdCut  
  
  
