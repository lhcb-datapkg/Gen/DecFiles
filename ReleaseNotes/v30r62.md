DecFiles v30r62 2021-08-05 
==========================  
 
! 2021-08-04 - Michal Kreps (MR !809)  
   Add 5 new decay files  
   + 14145100 : Bc_BuKS,JpsiK,mumu=BcVegPy,TightCut  
   + 11146501 : Bd_JpsiKSeta,mm,3pi=phsp,TightCut  
   + 11144501 : Bd_JpsiKSeta,mm,gg=phsp,TightCut  
   + 15146501 : Lb_JpsiLambdaeta,mm,3pi=phsp,TightCut  
   + 15144501 : Lb_JpsiLambdaeta,mm,gg=phsp,TightCut  
   Modify 2 decay files  
   + 15146500 : Lb_JpsiLambdaeta,mm,3pi=phsp,DecProdCut  
   + 15144500 : Lb_JpsiLambdaeta,mm,gg=phsp,DecProdCut  
  
! 2021-08-04 - Miguel Ramos Pernas (MR !808)  
   Add 2 new decay files  
   + 49000010 : HardQcdBgd,PT_14GeV,THETA_400mrad,NLT_50cm  
   + 49000011 : HardQcdBgd,PT_18GeV,THETA_400mrad,NLT_50cm  
  
! 2021-08-04 - Asier Pereiro Castro (MR !807)  
   Add new decay file  
   + 13104006 : Bs_Kst0Kst0=pTCuts,HelAmpsFromData  
  
! 2021-07-27 - Michel De Cian (MR !804)  
   Add 6 new decay files  
   + 11874403 : Bd_D0Xmunu,D0=cocktail,pipipi0,Dalitz,D0muInAcc  
   + 13874401 : Bs_Dsmunu,pipi0omega=cocktail,hqet2,TightCut,ForB2OmegaMuNu  
   + 13876400 : Bs_Dsmunu,pipipiomega=cocktail,hqet2,TightCut,ForB2OmegaMuNu  
   + 12873414 : Bu_D0Xmunu,D0=cocktail,pipipi0,Dalitz,D0muInAcc  
   + 12873502 : Bu_D0munu,KSomega=cocktail,BRcorr1,TightCut,ForB2OmegaMuNu  
   + 12873426 : Bu_D0munu,Kstaromega=cocktail,BRcorr1,TightCut,ForB2OmegaMuNu  
  
! 2021-07-26 - Harris Conan Bernstein (MR !803)  
   Add 3 new decay files  
   + 11198023 : Bd_DstDstKst0,Kpi,Kpi=TightCutNG  
   + 12197045 : Bu_DstD0Kst0,Kpi,Kpi=TightCut2NG  
   + 12197423 : Bu_DstDst0Kst0,Kpi,Kpi=TightCutNG  
  
! 2021-07-23 - Gary Robertson (MR !802)  
   Add 6 new decay files  
   + 26197970 : Pc4350,LcpiD+,pkpi=TightCut,InAcc  
   + 26197079 : Pc4350,Sigma_c0D+,Lcpi,pkpi=TightCut,InAcc  
   + 26197973 : Pc4450,LcpiD+,pkpi=TightCut,InAcc  
   + 26197971 : Pc4450,Sigma_c0D+,Lcpi,pkpi=TightCut,InAcc  
   + 26197974 : Pc4550,LcpiD+,pkpi=TightCut,InAcc  
   + 26197972 : Pc4550,Sigma_c0D+,Lcpi,pkpi=TightCut,InAcc  
  
! 2021-07-23 - Aravindhan Venkateswaran (MR !801)  
   Modify 3 decay files  
   + 11298010 : Bd_DDKst0,3piX=cocktail,TightCut  
   + 13298611 : Bs_DsDKst0,3piX=cocktail,TightCut  
   + 12297411 : Bu_D0DKst0,3piX=cocktail,TightCut  
  
! 2021-07-23 - Guy Henri Maurice Wormser (MR !800)  
   Add 2 new decay files  
   + 11898600 : Bd_nonresonantDstpipiXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut  
   + 12894600 : Bu_nonresonantDstpiXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut  
  
! 2021-05-14 - Albert Bursche (MR !746)  
   Add 4 new decay files  
   + 47202010 : exclu_rho,pipi=coherent_starlight,inter  
   + 47102000 : exclu_rho,pipi=coherent_starlight  
   + 47202011 : exclu_rho,pipi=incoherent_starlight,inter  
   + 47102001 : exclu_rho,pipi=incoherent_starlight  
  
! 2021-05-07 - Xiaokang Zhou (MR !734)  
   Add new decay file  
   + 35112021 : Xi0_pimu=DecProdCut  
  
! 2021-02-04 - Alison Maria Tully (MR !670)  
   Add 7 new decay files  
   + 14675034 : Bc_D0munu,Kpipipi=BcVegPy,TightDecProdCut,ffISGW2  
   + 14573033 : Bc_D0munu=BcVegPy,TightDecProdCut,ffISGW2  
   + 14573233 : Bc_Dst0munu,D0gamma,Kpi=BcVegPy,TightDecProdCut,ffISGW2  
   + 14675234 : Bc_Dst0munu,D0gamma,Kpipipi=BcVegPy,TightDecProdCut,ffISGW2  
   + 14573433 : Bc_Dst0munu,D0pi0,Kpi=BcVegPy,TightDecProdCut,ffISGW2  
   + 14675434 : Bc_Dst0munu,D0pi0,Kpipipi=BcVegPy,TightDecProdCut,ffISGW2  
   + 10063000 : incl_b=D0,Kpi,DecProdCut  
  
