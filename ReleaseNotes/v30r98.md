DecFiles v30r98 2023-10-25 
==========================  
 
! 2023-10-19 - Ching-Hua Li (MR !1659)  
   Add 3 new decay files  
   + 11584011 : Bd_Dst+taunu,D+pi,enunu=RDplusCut  
   + 11883000 : Bd_Dststtaunu,D0=cocktail,enunu=TightCuts  
   + 12883000 : Bu_Dststtaunu,D0=cocktail,enunu=TightCuts  
  
! 2023-10-13 - Ellinor Eckstein (MR !1656)  
   Add 4 new decay files  
   + 16166910 : Xib5945_Xibpi,Xib_Lc2595barLL,Lc2595bar_Lcbarpipi,Lcbar_pbarKpi=DecProdCut  
   + 16166019 : Xib5945_Xibpi,Xib_Lc2595barS,Lc2595bar_Lcbarpipi,Lcbar_pbarKpi,mS1875=DecProdCut  
   + 16166010 : Xib5945_Xibpi,Xib_Lc2595barXi0n,Lc2595bar_Lcbarpipi,Lcbar_pbarKpi=DecProdCut  
   + 16169130 : Xib_Lc2595barLL,Lc2595bar_Lcbarpipi,Lcbar_pbarKpi=DecProdCut  
  
! 2023-10-11 - Serena Maccolini (MR !1655)  
   Add 2 new decay files  
   + 21613070 : D+_pipimu+nu_mu=res,HighVisMass  
   + 25113033 : Lc+_p+mumu=phsp,TightCut  
  
! 2023-10-10 - Hanae Tilquin (MR !1654)  
   Add new decay file  
   + 13514075 : Bs_Ksttautau,mumu=DecProdCut  
  
! 2023-10-10 - Mengzhen Wang (MR !1653)  
   Add 3 new decay files  
   + 15576110 : Lb_Lcmunu,Lc_L3pi=TightCut  
   + 15574150 : Lb_Lcmunu,Lc_Lpi=TightCut  
   + 15678100 : Lb_Lcpipimunu,Lc_L3pi=TightCut  
  
! 2023-10-10 - Patrik Adlarson (MR !1652)  
   Add 2 new decay files  
   + 26514182 : Xic0_Ximmunu,L0pi,ppi=pshp,TightCut,DDD  
   + 26514181 : Xic0_Ximmunu,L0pi,ppi=pshp,TightCut,LLL_DDL  
  
! 2023-10-07 - Mark Smith (MR !1651)  
   Add new decay file  
   + 11574400 : Bd_Dstmunu,Kpipi0=HQET,DecProdCut  
  
! 2023-10-05 - Liangjun Xu (MR !1650)  
   Add new decay file  
   + 15204016 : Lb_pKKK=DecProdCut,PHSP,Charmless,Cocktail  
  
! 2023-10-04 - Zan Ren (MR !1649)  
   Add new decay file  
   + 16198040 : Xib_XicDspipi,pKpi,KKpi=DecProdCut  
  
! 2023-10-02 - Aravindhan Venkateswaran (MR !1648)  
   Add 2 new decay files  
   + 16145039 : X6170-_JpsiKpip,mumu=phsp,DecProdCut,PPChange  
   + 16145930 : X6255+_Jpsiphip,mumu=phsp,DecProdCut,PPChange  
  
! 2023-09-26 - Miroslav Saur (MR !1634)  
   Add 11 new decay files  
   + 16105139 : Ombm_OmmKS0,OmmToLambdaK,KsTopipi=TightCut,AngularCut  
   + 16107136 : Ombm_OmmKpKmPipPim,ppi=TightCut,AngularCut  
   + 16105136 : Ombm_OmmPhi,OmmToLambdaK,PhiToKK=TightCut,AngularCut  
   + 16107135 : Ombm_OmmPipPipPimPim,ppi=TightCut,AngularCut  
   + 16105930 : Xibm_XimKS,XimToLambdaPi,KSToPiPi=TightCut,AngularCut  
   + 16107132 : Xibm_XimKpKmPipPim,ppi=TightCut,AngularCut  
   + 16107134 : Xibm_XimKpKpKmKmm,ppi=TightCut,AngularCut  
   + 16107133 : Xibm_XimKpKpPimPim,ppi=TightCut,AngularCut  
   + 16107131 : Xibm_XimKpPipPimPim,ppi=TightCut,AngularCut  
   + 16105931 : Xibm_XimPhi,XimToLambdaPi,PhiToKK=TightCut,AngularCut  
   + 16107130 : Xibm_XimPipPipPimPim,ppi=TightCut,AngularCut  
  
! 2023-09-25 - Tianze Rong (MR !1633)  
   Add 4 new decay files  
   + 15196130 : Lb_LcDs,Lambdapi=DecProdCut  
   + 15196131 : Lb_LcDs,Lambdapi=TightCut  
   + 15196120 : Lb_LcDs,pKS=DecProdCut  
   + 15196121 : Lb_LcDs,pKS=TightCut  
  
! 2023-08-21 - Zeqing Mu (MR !1612)  
   Add new decay file  
   + 14167091 : Bc_DstKpi,D0pi,Kpipipi=BcVegPy,DecProdCut  
  
