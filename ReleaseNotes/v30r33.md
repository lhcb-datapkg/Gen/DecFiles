!========================= DecFiles v30r33 2019-07-23 =======================  
  
! 2019-07-22 - Iaroslava Bezshyiko (MR !325)  
   Add 3 new decay files  
   + 12575070 : Bu_Lcpbarmunu,pKpi=TightCut,v2  
   + 15876030 : Lb_Lc2765munu,pKpi=cocktail,TightCut,v2  
   + 15876031 : Lb_Lc2880munu,pKpi=cocktail,TightCut,v2  
  
! 2019-07-18 - Harris Conan Bernstein (MR !323)  
   Add 10 new decay files  
   + 11198005 : Bd_DDstKst0,Kpipi,Kpi=TightCut  
   + 11198400 : Bd_DDstKst0,Kpipi,Kpipi=TightCut  
   + 11198410 : Bd_DstDstKst0,Kpi,Kpipi=TightCut  
   + 11198401 : Bd_DstDstKst0,Kpipi,Kpipi=TightCut  
   + 13198040 : Bs_DsDKst0,KKpi,Kpipi=TightCut  
   + 13198400 : Bs_DsDstKst0,KKpi,Kpipi=TightCut  
   + 13198200 : Bs_DsstDKst0,KKpi,Kpipi=TightCut  
   + 13198600 : Bs_DsstDstKst0,KKpi,Kpipi=TightCut  
   + 12197400 : Bu_DstD0Kst0,Kpipi,Kpi=TightCut  
   + 12197401 : Bu_DstDst0Kst0,Kpipi,Kpi=TightCut  
  
! 2019-07-18 - Dmitrii Pereima (MR !322)  
   Add new decay file  
   + 12245043 : Bu_psi3823K,PHSP,Jpsipipi=TightCut  
  
! 2019-07-18 - Liupan An (MR !321)  
   Add new decay file  
   + 17164201 : Bdst_Bdgamma,Dpi,Kpipi=DecProdCut  
  
! 2019-07-16 - Paras Naik (MR !320)  
   Modified 2 decay files
   + 12165007 : Bu_D0K,KKpipi=DecProdCut,AmpGen
   + 12165033 : Bu_D0pi,KKpipi=DecProdCut,AmpGen
  
! 2019-07-15 - Timothy David Evans (MR !319)  
   Add 2 new decay files  
   + 15574041 : Lb_Lcmunu,pKK=AmpGen,DecProdCut  
   + 15574051 : Lb_Lcmunu,ppipi=AmpGen,DecProdCut  
  
! 2019-07-11 - Xuhao Yuan (MR !318)  
   Add new decay file  
   + 14543024 : Bc_psi2STauNu,mununu,mm=BcVegPy,ffEbert,DecProdCut  
  
! 2019-07-10 - Victor Renaudin (MR !317)  
   Add 6 new decay files  
   + 13166141 : Bs_D0Kpi,KSKK=sqDalitz,TightCut  
   + 13166140 : Bs_D0Kpi,KSpipi=sqDalitz,TightCut  
   + 13166321 : Bs_Dst0Kpi,D0gamma,KSKK=sqDalitz,TightCut  
   + 13166320 : Bs_Dst0Kpi,D0gamma,KSpipi=sqDalitz,TightCut  
   + 13166731 : Bs_Dst0Kpi,D0pi0,KSKK=sqDalitz,TightCut  
   + 13166730 : Bs_Dst0Kpi,D0pi0,KSpipi=sqDalitz,TightCut  
  
! 2019-07-10 - Mariia Poliakova (MR !316)  
   Add 2 new decay files  
   + 17812080 : Bs2st_BuK,Bu=cocktail,mm=DecProdCut  
   + 17876481 : Bs2st_BuK,D0XMuNu,K0pipi,D0=cocktail,mm=DecProdCut  
  
! 2019-06-21 - Stefano Cali (MR !305)  
   Add 11 new decay files  
   + 11896204 : Bd_DsstDst,DsgammaDpi,KKpimunuX=cocktail,mu3hInAcc,TightCut  
   + 13774625 : Bs_Ds1munu=ISGW2,mu3hInAcc  
   + 13774637 : Bs_Ds1taunu=ISGW2,mu3hInAcc  
   + 13774221 : Bs_Dsmunu=Ds+Dsst=hqet2,mu3hInAcc  
   + 13774423 : Bs_Dsmunu=Dsstst=cocktail,ISGW2,mu3hInAcc  
   + 13594613 : Bs_DsstDsst,DsgammaDsgamma,KKpimunuX=cocktail,mu3hInAcc,TightCut  
   + 13774232 : Bs_Dstaunu=Ds+Dsst=ISGW2,mu3hInAcc  
   + 13774434 : Bs_Dstaunu=Dsstst=cocktail,ISGW2,mu3hInAcc  
   + 12575200 : Bu_DsKmunu=Ds+Dsst=PHPS,mu3hInAcc  
   + 12895614 : Bu_DsstDst,DsgammaD0pi,KKpimunuX=cocktail,mu3hInAcc,TightCut  
   + 15694314 : Lb_DsstLcX,DsgammamunuX,KKpi=cocktail,mu3hInAcc,TightCut  
  
