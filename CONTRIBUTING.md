# Steps to add new decay files
1. Find if a DecFile you want (or similar enough so you can use it) already exists
1. Read the [event type numbering convention](https://cds.cern.ch/record/855452?ln=en). There is [an unofficial tool](http://lbeventtype.web.cern.ch/) to assist with creating an event type, but keep in mind that it is not a substitute for the documentation.
1. If you are using an old DecFile as a template, note that it might not follow the established conventions.
1. Test your DecFile (both that it runs and that it produces output you want)
1. Commit to a branch and create a merge request against master.
1. Open your merge request in web browser and check that all tests are successful. If anything fails, please correct and recheck again day after commit. If you do not understand failure, get in touch with [lhcb-sim-developers@cernNOSPAMPLEASE.ch](mailto://lhcb-sim-developers@cernNOSPAMPLEASE.ch)
1. **Make sure the CI test pass**. Make sure no warnings / errors pop up, if they do then either fix them, or understand them and add a comment in the merge request on why this popped up. See [below](#automatic-testing-of-a-decay-file) for more information about the CI test.
1. Watch discussion in merge request for any comments we might have.

> **Preparing a merge request for review**
>
> The current maintainers and reviewers are @kreps, @fabudine, and @emmuhamm.
>
> **Write a detailed header in the merge request** explaining which decay modes are being added and which studies (analysis, comissioning, etc) are the decay files going to be used for. 
> In particular, add clear and detailed explanations if you are applying custom generator-level selections or modifying particle properties. 
> When applicable, add references for the selections and modified particle properties.     
>
> **Pay attention to the CI test** (see detailed info [below](#automatic-testing-of-a-decay-file)); especially, check the decfile parser test.
> Address errors and warnings, either by fixing them or by commenting why you think the decparser might be wrong.
>
> **Mark the merge request as ready** to initiate the review process. Merge requests marked as draft will be ignored.
> Please refrain from assigning the maintainers as reviewers, as one of the maintainers will get to it in their time.
>
> Once the merge request has been approved, it will be merged and released by the next release cycle. Release cycles are targetted to be *roughly* once every two weeks.

# Branch to use
For new decay files to be used in Sim09 productions, one should target the `master` branch.
The decay files which are meant only for Sim10 should target the `Sim10` branch.
If the decay file should be available in both Sim09 and Sim10, and will work without modifications in both branches, target the `master` branch and make a note in the merge request specifying that it is required also for Sim10 (while decay files are regularly copied to Sim10 from Sim09, the comment will tell that we should do it without delay).

# Correct Descriptor usage in DecFiles
Decay files are based on the EvtGen descriptor syntax, see `Gen/DecFiles/scripts/evt.pdl` for naming conventions.
This EvtGen syntax is used:
1. In the second line of your decay file (starting with `Descriptor:`)
1. For the actual definition of your decay in the lower half of your decay file (starting with `Alias`, etc.).

If you are using generator-level cuts in a python-code insertion, you have to use the [DecayDescriptor syntax](https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders#Grammar_in_short) for this part:
1. Especially the correct usage of arrows (`->`, `=>`, `==>`, etc.) in `tightCut.Decay = '[...]'` is important.

# Testing a decay file
This is done in three steps as detailed below.

**NB** before starting, make sure that the LHCb user Environment (`LbEnv`) is enabled (in the normal case this is automatically done when logging into `lxplus`).

## 1) Write the decay file and create the options

First, get the Gen/DecFiles package from gitlab.
```shell
git lb-clone-pkg Gen/DecFiles
```

**NB** if you need a different branch, pass `-b`, e.g.:
```shell
git lb-clone-pkg Gen/DecFiles -b Sim10
```
Go to the `Decfiles` folder.
```shell
cd Gen/DecFiles
```
Add the new decay file in the `dkfiles` subdirectory (under `Gen/DecFiles/dkfiles`).
Then create the option file to be used by Gauss by compiling as below (to compile you have to be in the `Decfiles` folder). 
```shell
make
```
If there is no mistake in the decay file, an option file called `NNNNNNNN.py` will be present in the `options` subdirectory, where `NNNNNNNN` is the event-type number of the decay file.
It is also recommended to run the decay file convention parser
```shell
cd Gen/DecFiles/cmt
./decparser.sh ../dkfiles/DecFile.dec
```
This should check for convention correctness for the event type and some basic mistakes, but it also has some limitations. This is our tool to help in checking, but at the end all failures are individually considered. Feel free to email any comments to [lhcb-gauss-manager@cernNOSPAMPLEASE.ch](mailto://lhcb-gauss-manager@cernNOSPAMPLEASE.ch).

## 2) Run Gauss to create a .xgen file

If you do not already have the environment set up, prepare it to run one of the latest Gauss version (`v49rX` for Sim09 productions, or `v50rX` for upgrade productions).
In the examples below, `v49r25` is used.
```shell
cd Gen/DecFiles
lb-run Gauss/v49r25 bash
export DECFILESROOT=${PWD}
```

Now produce simulation with Gauss.
```shell
gaudirun.py $GAUSSOPTS/Gauss-Job.py $GAUSSOPTS/Gauss-2016.py $GAUSSOPTS/GenStandAlone.py \
            $DECFILESROOT/options/NNNNNNNN.py $LBPYTHIA8ROOT/options/Pythia8.py
```

If you need to produce simulation using `BcVegPy` (for Bc mesons), then replace in the line above `$LBPYTHIA8ROOT/options/Pythia8.py` with `$LBBCVEGPYROOT/options/BcVegPyPythia8.py`.

The number of events to be generated (5 by default) is set in the `Gauss-Job.py` file.  Gauss will then produce a file called by default `Gauss-NNNNNNNN-5ev-YYYYMMDD.xgen`, with a name formed from the event type (`NNNNNNNN`), the number of events (`5ev` by default) and the day (in format `YYYYMMDD`).
The above command includes spillover, which you should use for timing of your event type.
If for development reasons you want to remove the spillover, you can use `$GAUSSOPTS/Gauss-DEV.py` instead of `$GAUSSOPTS/Gauss-2016.py`.

The produced `.xgen` file contains the generator level information (both in HepMC and MCParticles format) when running Gauss with only the generator part, without the simulation and Geant4 being activated.

## 3) Create an MCDecayTreeTuple from the .xgen file
Use DaVinci to read the file produced by Gauss and to create a ROOT file with the information from the generator-level history.
```shell
lb-run -c best DaVinci/v46r10 gaudirun.py tupleResult.py
```
where tupleResult.py contains
```python
from Configurables import (
    DaVinci,
    EventSelector,
    PrintMCTree,
    MCDecayTreeTuple
)
from DecayTreeTuple.Configuration import *

"""Configure the variables below with:
decay: Decay you want to inspect, using 'newer' LoKi decay descriptor syntax,
decay_heads: Particles you'd like to see the decay tree of,
datafile: Where the file created by the Gauss generation phase is, and
year: What year the MC is simulating.
"""

# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
decay = "[B0 => ^(Lambda_c~- ==> ^p~- ^K+ ^pi-) ^p+ ^pi- ^pi+]CC"
decay_heads = ["B0", "B~0"]
datafile = "Gauss-11166070-5ev-20240923.xgen"
year = 2012

# For a quick and dirty check, you don't need to edit anything below here.
##########################################################################

# Create an MC DTT containing any candidates matching the decay descriptor
mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
mctuple.Decay = decay
mctuple.ToolList = [
    "MCTupleToolHierarchy",
    "LoKi::Hybrid::MCTupleTool/LoKi_Photos"
]
# Add a 'number of photons' branch
mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True
mctuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Photos").Variables = {
    "nPhotos": "MCNINTREE(('gamma' == MCABSID))"
}

# Print the decay tree for any particle in decay_heads
printMC = PrintMCTree()
printMC.ParticleNames = decay_heads

# Name of the .xgen file produced by Gauss
EventSelector().Input = ["DATAFILE='{0}' TYP='POOL_ROOTTREE' Opt='READ'".format(datafile)]

# Configure DaVinci
DaVinci().TupleFile = "DVntuple.root"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DataType = str(year)
DaVinci().UserAlgorithms = [printMC, mctuple]
```
If you get the error
```
ToolSvc.OdinTim...  FATAL ODINDecodeTool:: Exception throw: Cannot find RawEvent in [Trigger/RawEvent, DAQ/RawEvent] StatusCode=FAILURE
```
try adding this to your file (copied from https://gitlab.cern.ch/lhcb/Bender/blob/master/Phys/BenderTools/python/BenderTools/GenFiles.py):
```python
from Gaudi.Configuration import appendPostConfigAction
def doIt():
    """
    specific post-config action for (x)GEN-files
    """
    extension = "xgen"
    ext = extension.upper()

    from Configurables import DataOnDemandSvc
    dod  = DataOnDemandSvc ()
    from copy import deepcopy
    algs = deepcopy ( dod.AlgMap )
    bad  = set()
    for key in algs :
        if     0 <= key.find ( 'Rec'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Raw'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'DAQ'     )                  : bad.add ( key )
        elif   0 <= key.find ( 'Trigger' )                  : bad.add ( key )
        elif   0 <= key.find ( 'Phys'    )                  : bad.add ( key )
        elif   0 <= key.find ( 'Prev/'   )                  : bad.add ( key )
        elif   0 <= key.find ( 'Next/'   )                  : bad.add ( key )
        elif   0 <= key.find ( '/MC/'    ) and 'GEN' == ext : bad.add ( key )

    for b in bad :
        del algs[b]

    dod.AlgMap = algs

    from Configurables import EventClockSvc, CondDB
    EventClockSvc ( EventTimeDecoder = "FakeEventTime" )
    CondDB  ( IgnoreHeartBeat = True )

appendPostConfigAction( doIt )
```

# Efficiency of generator-level cuts
This number can be extracted from the GeneratorLog.xml file (produced by your gauss job), where the printout is of the form
```xml
<efficiency name = "generator level cut">
    <after> 5 </after>
    <before> 27 </before>
    <value> 0.18519 </value>
    <error> 0.074757 </error>
  </efficiency>
```
the efficiency is therefore 5/27 in this example.


# Commiting a decay file
Pre-requisite: have account on gitlab.cern.ch
You must commit your decay file to separate branch and create a merge request against the master branch. It is recommended that you check out a clean version
```shell
git lb-clone-pkg  Gen/DecFiles
cd Gen/DecFiles
git checkout -b ${USER}/my-changes
```
Copy your decfile(s) into `dkfiles/` and add them to commit
```shell
git add dkfiles/[your decfile]
```
Commit everything, push to the gitlab server
```shell
git status
git commit -m "Added new file for XY decays." #(with a commit message explaining what you added)
git push -u origin ${USER}/my-changes
```
Go to [gitlab.cern.ch](https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles) and create a merge request.

If you find out that something in the merge request needs to be updated, just commit and push to the same branch.
The merge request will be automatically updated.
There is no need to close the merge request and start a new one; it is discouraged as it splits the discussion.

# Automatic testing of a decay file
When you create a merge request or update an existing one with a new commit, an automatic test will be performed. You can find the results of the test directly on the gitlab page of your merge request under a text like "Pipeline #nnnnnnn passed/failed ...". You can click on it and see the details of all tests. The test runs in three steps: it generates option files, runs the decparser and then runs Gauss itself for a few events. If you see a failure, please fix the issue. If you do not understand the failure, get in touch with [lhcb-gauss-manager@cernNOSPAMPLEASE.ch](mailto://lhcb-gauss-manager@cernNOSPAMPLEASE.ch). The merge requests cannot be merged until it passes all tests.
Do not assume that somebody will remind that it needs to be done (or fix it behind the scenes).

The CI test contains three states when it parses the decay file:
1. Errors
2. Warnings
3. Cautions

Errors represent important failures, and should almost always be fixed before the merge request gets merged. These are picked up by the CI test and will result in a failed CI output (red X), and are printed in red in the logs. If you are sure that the error is well understood, leave a comment explaining so.

Warnings represent important information and should be also fixed before the merge request gets merged, but they are less dangerous than the errors. These will result in a passed CI test with warning CI output (yellow!), and are represented as orange warnings in the log. If you are sure that the error is well understood, leave a comment explaining so.

Cautions are information that might be nice to fix, but won't block merges if they occur. They will result in a passed CI output, represented as a passed CI output with a green tick, and are represented with a yellow caution in the log.

# Potential issues
If you see a problem with "Inclusive&Marked decay in LoKi", try to check https://gitlab.cern.ch/lhcb/LHCb/-/issues/113 and look at the example showing how it was resolved at https://gitlab.cern.ch/lhcb-datapkg/Gen/DecFiles/-/merge_requests/775.

